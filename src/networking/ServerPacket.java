package networking;

import java.io.Serializable;

public class ServerPacket implements Serializable {
	private static final long serialVersionUID = -273298044628957355L;
	public boolean success;
  	public Object object;
  	public String message;

  	public enum ResultType {
	  SIGN, LOBBY, ROOM, GAME, OVER
  	}

  	public ResultType type;

  	public ServerPacket(ResultType _type, boolean _success) {
  		type = _type;
    	success = _success;
    	object = null;
    	message = "";
  	}
  
  	public ServerPacket(ResultType _type, Object _object, String _message) {
  		type = _type;
  		object = _object;
  		message = _message;
  		success = true;
  	}
}
