package networking;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.Vector;

import gameObject.Bullet;
import gameObject.GameBoard;
import gameObject.Tank;
import gameObject.TankColor;
import gameObject.Tree;
import gameObject.Wall;
import gameObject.Water;
import util.Constants;
import server.GameEngine;
import server.PlayerRecord;

public class CompressedBoard implements Serializable {
	private static final long serialVersionUID = 8170370206886227269L;
	public transient GameBoard board;
	public char[] compressed;
	
	public CompressedBoard(char[] comp) {
		compressed = comp;
		decompressBoard();
	}
	
	public CompressedBoard(GameBoard board) {
		int length = 5+board.getTanks().size()*53;
		int numObjs = 0;
		for (Bullet bullet : board.getBullets()) {
			if (!bullet.isDestroyed())
				numObjs++;
		}
		length += 1+numObjs*8;
		//System.out.print("bullets "+numObjs);
		numObjs = 0;
		for (Tree tree : board.getTrees()) {
			if (!tree.isDestroyed())
				numObjs++;
		}
		length += 1+board.getWalls().size()*6+1+board.getWaters().size()*7+1+numObjs*7;
		length += 20;
		//System.out.print(" total length "+length+" ");
		compressed = new char[length];
		//push bg
		compressed[0] = (char)board.getBackgroundInt();
		//sound
		if (board.beepSound)
			compressed[1] = 1;
		else if (board.hitTankSound)
			compressed[1] = 2;
		else if (board.hitTreeSound)
			compressed[1] = 3;
		else if (board.hitWallSound)
			compressed[1] = 4;
		else if (board.shootSound)
			compressed[1] = 5;
		else if (board.startSound)
			compressed[1] = 6;
		else 
			compressed[1] = 0;
		//time
		long time = board.getTime()/100;
		compressed[2] = (char)(time/256);
		compressed[3] = (char)(time%256);
		//tank num
		Vector<Tank> tanks = board.getTanks();
		compressed[4] = (char)tanks.size();
		int pos = 5;
		for (int i = 0; i < tanks.size(); i++) {
			Tank tank = tanks.get(i);
			
			String name = tank.getMyPlayerRecord().name;
			for (int j = 0; j < name.length(); j++) {
				compressed[pos+j] = name.charAt(j);
			}
			for (int j = name.length(); j < 40; j++)
				compressed[pos+j] = 0;
			pos += 40;
			
			int score = tank.getMyPlayerRecord().score;
			int pic = util.Util.levelFromScore(score)*4;
			if (tank.getMyPlayerRecord().color == TankColor.Type.Red) {
				pic += 0;
			} else if (tank.getMyPlayerRecord().color == TankColor.Type.Yellow) {
				pic += 1;
			} else if (tank.getMyPlayerRecord().color == TankColor.Type.Green) {
				pic += 2;
			} else if (tank.getMyPlayerRecord().color == TankColor.Type.Blue) {
				pic += 3;
			}
			compressed[pos] = (char)pic;
			pos++;
			
			pushPositionAngle(pos, tank.getShape().getPosition(), tank.getOffset(), tank.getShape().getAngle());
			pos += 8;
			
			compressed[pos] = (char)(tank.getKills());
			pos++;
			compressed[pos] = (char)(tank.getDeaths());
			pos++;
			
			long reload = tank.getReload();
			char percent = (char)((double)255.0*reload/Constants.ReloadTime);
			compressed[pos] = percent;
			pos++;
			
			if (!tank.isAlive())
				compressed[pos] = 0;
			else if (!tank.isVulnerable())
				compressed[pos] = 1;
			else
				compressed[pos] = 2;
			pos++;
		}
		
		Vector<Bullet> bullets = board.getBullets();
		compressed[pos] = (char)bullets.size();
		int bulletPos = pos;
		pos++;
		for (int i = 0; i < bullets.size(); i++) {
			Bullet bullet = bullets.get(i);
			if (bullet.isDestroyed()) {
				compressed[bulletPos]--;
				continue;
			}
			pushPositionAngle(pos, bullet.getShape().getPosition(), bullet.getOffset(), bullet.getShape().getAngle());
			pos += 8;
		}
		Vector<Wall> walls = board.getWalls();
		compressed[pos] = (char)walls.size();
		pos++;
		for (int i = 0; i < walls.size(); i++) {
			Wall wall = walls.get(i);
			pushPositionAngle(pos, wall.getPosition(), wall.getOffset(), 0);
			pos += 6;
		}
		Vector<Water> waters = board.getWaters();
		compressed[pos] = (char)waters.size();
		pos++;
		for (int i = 0; i < waters.size(); i++) {
			Water water = waters.get(i);
			compressed[pos] = (char)water.getObjectIdx();
			pos++;
			pushPositionAngle(pos, water.getPosition(), water.getOffset(), 0);
			pos += 6;
		}
		Vector<Tree> trees = board.getTrees();
		compressed[pos] = (char)trees.size();
		int treePos = pos;
		pos++;
		for (int i = 0; i < trees.size(); i++) {
			Tree tree = trees.get(i);
			if (tree.isDestroyed()) {
				compressed[treePos]--;
				continue;
			}
			compressed[pos] = (char)tree.getObjectIdx();
			pos++;
			pushPositionAngle(pos, tree.getPosition(), tree.getOffset(), 0);
			pos += 6;
		}
		if (board.getState() == GameEngine.State.Three) {
			compressed[pos] = 3;
		} else if (board.getState() == GameEngine.State.Two) {
			compressed[pos] = 2;
		} else if (board.getState() == GameEngine.State.One) {
			compressed[pos] = 1;
		} else {
			compressed[pos] = 0;
		}
		//System.out.println(""+pos+" bytes");
	}
	
	private void pushPositionAngle(int pos, Point2D.Double point, Point2D.Double ofs, double angle) {
		int x = (int)point.getX();
		int y = (int)point.getY();
		compressed[pos] = (char)(x/256);
		compressed[pos+1] = (char)(x%256);
		compressed[pos+2] = (char)(y/256);
		compressed[pos+3] = (char)(y%256);
		int ofx = (int)ofs.getX();
		int ofy = (int)ofs.getY();
		compressed[pos+4] = (char)((int)ofx);
		compressed[pos+5] = (char)((int)ofy);
		int a = (int)angle;
		while (a < 0)
			a += 360;
		compressed[pos+6] = (char)(a/256);
		compressed[pos+7] = (char)(a%256);
	}
	
	public void decompressBoard() {
		board = new GameBoard();
		//Background
		board.setBackground((int) compressed[0]);
		
		//Sounds
		board.beepSound = compressed[1] == 1;
		board.hitTankSound = compressed[1] == 2;
		board.hitTreeSound = compressed[1] == 3;
		board.hitWallSound = compressed[1] == 4;
		board.shootSound = compressed[1] == 5;
		board.startSound = compressed[1] == 6;
		
		//time
		board.setTime(100 * (((long) compressed[2]) * 256 + ((long) compressed[3]))); 
		
		//Tanks
		int tank_count = (int) compressed[4];
		int pos = 5;
		for (int i = 0; i < tank_count; ++i) {
			PlayerRecord pr = new PlayerRecord();
			
			//Name
			String name = "";
			int prev_pos = pos;
			while (compressed[pos] != 0) {
				name += compressed[pos];
				++pos;
			}
			pr.name = name;
			pos = prev_pos + 40;
			
			//Color
			if (compressed[pos] % 4 == 0) {
				pr.color = TankColor.Type.Red;
			} else if (compressed[pos] % 4 == 1) {
				pr.color = TankColor.Type.Yellow;
			} else if (compressed[pos] % 4 == 2) {
				pr.color = TankColor.Type.Green;
			} else if (compressed[pos] % 4 == 3) {
				pr.color = TankColor.Type.Blue;
			}
			
			//Fake level 
			pr.score = (compressed[pos] / 4 - 1) * 1000;
			++pos;
	
			//Tank
			//Position
			double x = 256 * compressed[pos] + compressed[pos + 1];
			double y = 256 * compressed[pos + 2] + compressed[pos + 3];
	
			Tank t = new Tank(pr, new Point2D.Double(x, y), 0); 
			pos += 4;
			
			//Offset
			double ofx = compressed[pos];
			double ofy = compressed[pos + 1];
			t.setOffset(new Point2D.Double(ofx, ofy));
			pos += 2;
			
			//Angle
			t.setAngle(256 * compressed[pos] + compressed[pos + 1]);
			pos += 2;
			
			//Kills and deaths
			t.setKills(compressed[pos]);
			t.setDeaths(compressed[pos + 1]);
			pos += 2;
			
			//Reload
			t.setReload((long) (Constants.ReloadTime * compressed[pos] / 255.0));
			pos += 1;
			
			//Dead or Alive
			if (compressed[pos] == 0)
				t.setRespawn();
			else if (compressed[pos] == 1)
				t.setInvincible();
			else if (compressed[pos] == 2){
			}
			
			board.addTank(t);
			pos += 1;
		}			
		
		//Bullet 
		int bullet_count = compressed[pos];
		pos += 1;
		for (int i = 0; i < bullet_count; ++i) {
			//Position
			double x = 256 * compressed[pos] + compressed[pos + 1];
			double y = 256 * compressed[pos + 2] + compressed[pos + 3];
			pos += 4;
			
			//Offset
			double ofx = compressed[pos];
			double ofy = compressed[pos + 1];
			pos += 2;
			
			//Angle
			double angle = 256 * compressed[pos] + compressed[pos + 1];
			pos += 2;
			
			Bullet b = new Bullet(angle, new Point2D.Double(x, y), 0);
			b.setOffset(new Point2D.Double(ofx, ofy));
			
			board.addBullet(b);
		}
		
		//Wall
		int wall_count = compressed[pos];
		pos += 1;
		for (int i = 0; i < wall_count; ++i) {
			//Position
			double x = 256 * compressed[pos] + compressed[pos + 1];
			double y = 256 * compressed[pos + 2] + compressed[pos + 3];
			pos += 4;
			
			//Offset
			double ofx = compressed[pos];
			double ofy = compressed[pos + 1];
			pos += 2;

			Wall wl = new Wall(new Point2D.Double(x, y));
			wl.setOffset(new Point2D.Double(ofx, ofy));
			board.addWall(wl);
		}
		
		//Water
		int water_count = compressed[pos];
		pos += 1;
		for (int i = 0; i < water_count; ++i) {
			//Type
			int type = compressed[pos];
			pos += 1;
			
			//Position
			double x = 256 * compressed[pos] + compressed[pos + 1];
			double y = 256 * compressed[pos + 2] + compressed[pos + 3];
			pos += 4;
			
			//Offset
			double ofx = compressed[pos];
			double ofy = compressed[pos + 1];
			pos += 2;

			Water wr = new Water(new Point2D.Double(x, y));
			wr.setObjectIdx(type);
			wr.setOffset(new Point2D.Double(ofx, ofy));
			board.addWater(wr);
		}
		
		//Tree
		int tree_count = compressed[pos];
		pos += 1;
		for (int i = 0; i < tree_count; ++i) {
			//Type
			int type = compressed[pos];
			pos += 1;
			
			//Position
			double x = 256 * compressed[pos] + compressed[pos + 1];
			double y = 256 * compressed[pos + 2] + compressed[pos + 3];
			pos += 4;
			
			//Offset
			double ofx = compressed[pos];
			double ofy = compressed[pos + 1];
			pos += 2;

			Tree tr = new Tree(new Point2D.Double(x, y));
			tr.setObjectIdx(type);
			tr.setOffset(new Point2D.Double(ofx, ofy));
			board.addTree(tr);
		}
		if (compressed[pos] == 3) {
			board.setState(GameEngine.State.Three);
		} else if (compressed[pos] == 2) {
			board.setState(GameEngine.State.Two);
		} else if (compressed[pos] == 1) {
			board.setState(GameEngine.State.One);
		}
	}
}
