package networking;

import java.io.Serializable;
import java.security.MessageDigest;

public class SignIn implements Serializable {
  private static final long serialVersionUID = 1L;
  public String name, hash;
  public boolean signup;

  public SignIn(String _name, char[] _pass, boolean _signup) {
    name = _name.toLowerCase();
    try {
      MessageDigest md = MessageDigest.getInstance("SHA-256");
      String text = new String(_pass);
      md.update(text.getBytes("UTF-8"));
      byte[] digest = md.digest();
      hash = String.format("%064x", new java.math.BigInteger(1, digest));
    } catch (Exception e) {
      e.printStackTrace();
    }
    signup = _signup;
  }

  public static boolean validateName(String name) {
    for (int i = 0; i < name.length(); i++) {
      if (!Character.isAlphabetic(name.charAt(i)) && !Character.isDigit(name.charAt(i))) {
        return false;
      }
    }
    return true;
  }

  public static boolean validatePassword(char[] pass) {
    if (pass == null || pass.length == 0)
      return false;
    boolean upper = false;
    boolean digit = false;
    for (int i = 0; i < pass.length; i++) {
      if (Character.isUpperCase(pass[i])) {
        upper = true;
      } else if (Character.isDigit(pass[i])) {
        digit = true;
      } else if (!Character.isAlphabetic(pass[i]) && !Character.isDigit(pass[i])) {
        return false;
      }
    }
    return upper && digit;
  }

  private boolean validateHash(String hash) {
    return hash.matches("[A-Fa-f0-9]{64}");
  }

  public boolean isValid() {
    return validateName(name) && validateHash(hash);
  }
}
