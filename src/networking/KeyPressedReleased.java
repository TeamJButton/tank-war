package networking;

import java.io.Serializable;

import server.PlayerKey;

public class KeyPressedReleased implements Serializable {
  private static final long serialVersionUID = 1L;

  public enum Type {
    Pressed, Released
  }

  public PlayerKey key;
  public Type type;

  public KeyPressedReleased(PlayerKey _key, Type _type) {
    key = _key;
    type = _type;
  }
}
