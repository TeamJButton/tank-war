package networking;

import java.io.Serializable;

public class ClientPacket implements Serializable {
  private static final long serialVersionUID = 1L;

  public enum ResultType {
    SIGNIN, GUEST, SIGNOUT, CREATE_ROOM, KICK, COLOR, JOIN_ROOM, READY, NOT_READY, QUIT_ROOM, KEY_PRESS, END
  }

  public ResultType type;
  public Object object;

  public ClientPacket(ResultType _type, Object _object) {
    type = _type;
    object = _object;
  }
}
