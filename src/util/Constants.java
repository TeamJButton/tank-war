package util;

import java.awt.Font;
import java.awt.Image;
import java.awt.geom.Point2D;
import java.util.Random;

import gameObject.TankColor;


public class Constants {
  private static Random rand = new Random();

  public static final int ServerWidth = 600;
  public static final int ServerHeight = 600;
  public static final int GameBoardWidth = 720;
  public static final int GameBoardGUIWidth = 960;
  public static final int GameBoardHeight = 720;
  public static final int port = 8964;
//public static final String host = "localhost";
  public static final String host2 = "45.55.110.230"; //yifan
  public static final String host = "52.39.10.115"; //aws

  public static final Font ArialItalic = new Font("Arial", Font.ITALIC, 30);
  public static final Font ArialPlain = new Font("Arial", Font.PLAIN, 20);
  public static final Font ArialBlack = new Font("Arial Black", Font.BOLD, 30);
  public static final Font ArialBlackLarge = new Font("Arial Black", Font.BOLD, 60);
  public static final int GameCycle = 20;
  public static final int GameSendCycle = 40;
  public static final int WallLength = 70;
  
  public static final String WallSound = "wall.wav";
  public static final String TreeSound = "tree.wav";
  public static final String TankSound = "tank.wav";
  public static final String BulletSound = "bullet.wav";
  public static final String BeepSound = "notification.wav";
  public static final String StartSound = "start.wav";
  
  public static final int BulletWidth = 5;
  public static final int BulletHeight = 15;
  
  public static final int MaxLevel = 8;

  public static String getTankLevelColor(int level, TankColor.Type type) {
	  if (level > MaxLevel) {
		  level = MaxLevel;
	  }
	  String color = "";
	  if (type == TankColor.Type.Blue)
		  color = "blue";
	  else if (type == TankColor.Type.Green)
		  color = "green";
	  else if (type == TankColor.Type.Red)
		  color = "red";
	  else if (type == TankColor.Type.Yellow)
		  color = "yellow";
	  return "tanks/" + level + "" + color + ".png";
  }

  public static Image getTank(int level, TankColor.Type type) {
    return Util.loadImage(getTankLevelColor(level, type));
  }


  public static final long ReloadTime = 500;
  public static final long RespawnDuration = 2000; // ms
  public static final long InvincibilityDuration = 4000; // ms
  public static final int GameDuration = 90000; // 1.5 minutes
  public static final int MinTreeCount = 8;
  public static final int MaxTreeCount = 20;
  public static final int MinWallCount = 2;
  public static final int MaxWallCount = 5;
  public static final int MinWaterCount = 3;
  public static final int MaxWaterCount = 10;
  public static final boolean DrawShape = false;

  public static int getRandomInt(int n) {
    return rand.nextInt(n);
  }

  public static int getRandomBackground() {
    return rand.nextInt(Gameboards.length);
  }

  // public static Image getButtonBackground() {
  // return Util.loadImage("button/button.jpg").getScaledInstance(
  // 300, 300, Image.SCALE_DEFAULT);
  // }


  public static final String[] Gameboards =
      {"gameboard1.png", "gameboard2.png", "gameboard3.png", "gameboard4.png", "gameboard5.png"};

  public static Image getBackground(int idx) {
    return util.Util.loadImage("gameboard/" + Gameboards[idx]);
  }

  // public static Image getBullet() {
  // Image img = util.Util.loadImage("bullet/bullet.png");
  // return img.getScaledInstance(30, 30, Image.SCALE_DEFAULT);
  // }

  public static int getRandomTree() {
    return rand.nextInt(Trees.length);
  }

  private static final String[] Trees =
      {"tree1.png", "tree2.png", "tree3.png", "tree4.png", "tree5.png"};

  // public static Image getTree(int idx) {
  // return util.Util.loadImage("tree/" + Trees[idx]);
  // }

  public static int getRandomWall() {
    return rand.nextInt(Walls.length);
  }

  private static final String[] Walls = {"wall1.png"};

  // public static Image getWall(int idx) {
  // return util.Util.loadImage("wall/" + Walls[idx]);
  // }

  public static int getRandomWater() {
    return rand.nextInt(Waters.length);
  }

  private static final String[] Waters = {"water1.png", "water2.png", "water3.png"};

  // public static Image getWater(int idx) {
  // return util.Util.loadImage("water/" + Waters[idx]);
  // }

  public static String getRandomName() {
    return FirstNames[rand.nextInt(FirstNames.length)] + " "
        + LastNames[rand.nextInt(LastNames.length)];
  }

  private static final String[] FirstNames = {"Adaptable", "Adventurous", "Affable", "Affectionate",
      "Agreeable", "Ambitious", "Amiable", "Amicable", "Amusing", "Brave", "Bright", "Broad-minded",
      "Calm", "Careful", "Charming", "Communicative", "Compassionate", "Conscientious",
      "Considerate", "Convivial", "Courageous", "Courteous", "Creative", "Decisive", "Determined",
      "Diligent", "Diplomatic", "Discreet", "Dynamic", "Easygoing", "Emotional", "Energetic",
      "Enthusiastic", "Exuberant", "Fair-minded", "Faithful", "Fearless", "Forceful", "Frank",
      "Friendly", "Funny", "Fuzzy", "Generous", "Gentle", "Good", "Gregarious", "Hard-working",
      "Helpful", "Honest", "Humorous", "Imaginative", "Impartial", "Independent", "Intellectual",
      "Intelligent", "Intuitive", "Inventive", "Kind", "Loving", "Loyal", "Modest", "Neat", "Nice",
      "Optimistic", "Passionate", "Patient", "Persistent", "Pioneering", "Philosophical", "Placid",
      "Plucky", "Polite", "Powerful", "Practical", "Pro-active", "Quick-witted", "Quiet",
      "Rational", "Reliable", "Reserved", "Resourceful", "Romantic", "Self-confident",
      "Self-disciplined", "Sensible", "Sensitive", "Shy", "Sincere", "Sociable", "Straightforward",
      "Sympathetic", "Thoughtful", "Tidy", "Tough", "Unassuming", "Understanding", "Versatile",
      "Warmhearted", "Willing", "Warmhearted", "Willing", "Witty"};

  private static final String[] LastNames = {"Aardvark", "Albatross", "Alligator", "Alpaca", "Ant",
      "Anteater", "Antelope", "Ape", "Armadillo", "Ass", "Baboon", "Badger", "Barracuda", "Bat",
      "Bear", "Beaver", "Bee", "Bison", "Boar", "Buffalo", "Galago", "Butterfly", "Camel",
      "Caribou", "Cat", "Caterpillar", "Cattle", "Chamois", "Cheetah", "Chicken", "Chimpanzee",
      "Chinchilla", "Chough", "Clam", "Cobra", "Cockroach", "Cod", "Cormorant", "Coyote", "Crab",
      "Crane", "Crocodile", "Crow", "Curlew", "Deer", "Dinosaur", "Dog", "Dogfish", "Dolphin",
      "Donkey", "Dotterel", "Dove", "Dragonfly", "Duck", "Dugong", "Dunlin", "Eagle", "Echidna",
      "Eel", "Eland", "Elephant", "Elephant", "Seal", "Elk", "Emu", "Falcon", "Ferret", "Finch",
      "Fish", "Flamingo", "Fly", "Fox", "Frog", "Gaur", "Gazelle", "Gerbil", "Giant", "Panda",
      "Giraffe", "Gnat", "Gnu", "Goat", "Goose", "Goldfinch", "Goldfish", "Gorilla", "Goshawk",
      "Grasshopper", "Grouse", "Guanaco", "Guinea Fowl", "Guinea Pig", "Gull", "Hamster",
      "Hare", "Hawk", "Hedgehog", "Heron", "Herring", "Hippopotamus", "Hornet", "Horse", "Human",
      "Hummingbird", "Hyena", "Jackal", "Jaguar", "Jay", "Jay,", "Blue", "Jellyfish", "Kangaroo",
      "Koala", "Komodo dragon", "Kouprey", "Kudu", "Lapwing", "Lark", "Lemur", "Leopard", "Lion",
      "Llama", "Lobster", "Locust", "Loris", "Louse", "Lyrebird", "Magpie", "Mallard", "Manatee",
      "Marten", "Meerkat", "Mink", "Mole", "Monkey", "Moose", "Mouse", "Mosquito", "Mule",
      "Narwhal", "Newt", "Nightingale", "Octopus", "Okapi", "Opossum", "Oryx", "Ostrich", "Otter",
      "Owl", "Ox", "Oyster", "Panther", "Parrot", "Partridge", "Peafowl", "Pelican", "Penguin",
      "Pheasant", "Pig", "Pigeon", "Pony", "Porcupine", "Porpoise", "Prairie", "Dog", "Quail",
      "Quelea", "Rabbit", "Raccoon", "Rail", "Ram", "Rat", "Raven", "Red", "deer", "Red", "panda",
      "Reindeer", "Rhinoceros", "Rook", "Ruff", "Salamander", "Salmon", "Sand", "Dollar",
      "Sandpiper", "Sardine", "Scorpion", "Sea", "lion", "Sea", "Urchin", "Seahorse", "Seal",
      "Shark", "Sheep", "Shrew", "Shrimp", "Skunk", "Snail", "Snake", "Spider", "Squid", "Squirrel",
      "Starling", "Stingray", "Stinkbug", "Stork", "Swallow", "Swan", "Tapir", "Tarsier", "Termite",
      "Tiger", "Toad", "Trout", "Turkey", "Turtle", "Vicuña", "Viper", "Vulture", "Wallaby",
      "Walrus", "Wasp", "Water", "buffalo", "Weasel", "Whale", "Wolf", "Wolverine", "Wombat",
      "Woodcock", "Woodpecker", "Worm", "Wren", "Yak", "Zebra"};

  private static final int pad = 80;
  // TODO Determine starting coordinates in corners
  public static final Point2D.Double[] startingPoints =
      {new Point2D.Double(pad, pad), new Point2D.Double(Constants.GameBoardWidth - pad, pad),
          new Point2D.Double(pad, Constants.GameBoardHeight - pad),
          new Point2D.Double(Constants.GameBoardWidth - pad, Constants.GameBoardHeight - pad)};
  public static final double[] startingOrientations = {180, 180, 0, 0};

  public static final int[] rankScore = {200, 100, 50, 0};
}
