package util;

import java.awt.Image;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;

public class Util {
	// Game constants
	/* 90 degrees per second */
	public static final double angSpd = 160;
	public static final double tankSpd = 200;
	public static final double bulletSpd = 400;

	public static enum Transformation {
		Forward, Backward, Right, Left, ForwardRight, ForwardLeft, BackwardRight, BackwardLeft, Still
	}

	public static List<Image> setIcon() {
		List<Image> icons;
		icons = new ArrayList<>();
		icons.add(util.Util.loadImage("icon/p256.png"));
		icons.add(util.Util.loadImage("icon/p128.png"));
		icons.add(util.Util.loadImage("icon/p96.png"));
		icons.add(util.Util.loadImage("icon/p48.png"));
		icons.add(util.Util.loadImage("icon/p32.png"));
		icons.add(util.Util.loadImage("icon/p16.png"));
		return icons;
	}

	// TODO: Load Image Using ImageIO
	/**
	 * loadImage
	 * 
	 * @param dir
	 *            Path of the image file
	 * @return {@code Image} if read image successfully; null otherwise
	 * @author Yifan Meng
	 */
	public static Image loadImage(String dir) {
		Image img = null;
		try {
			// img = ImageIO.read(new File("resources/img/" + dir));
			img = ImageIO.read(Util.class.getResource("/img/" + dir));
		} catch (IOException ioe) {
			System.out.println("Exception when loading \"" + dir + "\": " + ioe.toString());
		}
		return img;
	}

	public static Object serializeFromString(String s) {
		byte[] data = DatatypeConverter.parseBase64Binary(s);
		ObjectInputStream ois;
		try {
			ois = new ObjectInputStream(new ByteArrayInputStream(data));
			Object o = ois.readObject();
			ois.close();
			return o;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** Write the object to a Base64 string. */
	public static String serializeToString(Serializable o) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos;
		try {
			oos = new ObjectOutputStream(baos);
			oos.writeObject(o);
			oos.close();
			return new String(DatatypeConverter.printBase64Binary(baos.toByteArray()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static int levelFromScore(int score) {
		int level = score / 1000 + 1;
		if (level > 9)
			level = 9;
		return level;
	}
}