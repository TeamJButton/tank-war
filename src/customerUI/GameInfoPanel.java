package customerUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Vector;

import javax.swing.JPanel;

import client.CacheImage;
import gameObject.Tank;
import util.Constants;

public class GameInfoPanel extends JPanel {
	private static final long serialVersionUID = 2259928048408747009L;
	private CacheImage cache;
	private Vector<Tank> tanks;
	private long time;
	private double reload;
	public GameInfoPanel(CacheImage _cache) {
		super();
		setLayout(new BorderLayout());
		cache = _cache;
		tanks = new Vector<Tank>();
	}
	
	public void setTanks(Vector<Tank> _tanks) {
		tanks = _tanks;
	}
	
	public void setTime(long _time) {
		time = _time;
	}
	
	public void setReload(double _percent) {
		reload = 1-_percent;
	}
	
	protected void paintComponent(Graphics g) {
		g.drawImage(cache.getPanelBackground(), Constants.GameBoardWidth, 0, null);
		final int left = 20 + Constants.GameBoardWidth;
		final int topPad = 30;
		final int indented = 20+left;
		final int lineHeight = 30;
		final int gridPad = 110;
		final int barLength = Constants.GameBoardGUIWidth-20-left;
		Font namefont = Constants.ArialBlack.deriveFont((float)20.0);
		g.setColor(Color.black);
		for (int i = 0; i < tanks.size(); i++) {
			g.setFont(namefont);
			g.drawString(tanks.get(i).getMyPlayerRecord().name, left, topPad+gridPad*i+lineHeight);
			g.setFont(Constants.ArialPlain);
			g.drawString("Kills  "+tanks.get(i).getKills(), indented, topPad+gridPad*i+lineHeight*2);
			g.drawString("Deaths "+tanks.get(i).getDeaths(), indented, topPad+gridPad*i+lineHeight*3);
		}
		String timestr = "Time " + time/1000+"."+(time/100)%10;
		g.drawString(timestr, left, Constants.GameBoardHeight-70);
		g.drawString("Reload ", left, Constants.GameBoardHeight-40);
		if (reload >= 0.999)
			g.setColor(Color.green);
		else
			g.setColor(Color.red);
		g.fillRect(left, Constants.GameBoardHeight-35, (int)(barLength*reload), 20);
	}
}
