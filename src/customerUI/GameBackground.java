package customerUI;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.util.Vector;

import javax.swing.JPanel;

import client.CacheImage;
import gameObject.Wall;
import gameObject.Water;
import util.Constants;

public class GameBackground extends JPanel {
	private static final long serialVersionUID = -3538551433690593895L;
	private int bg;
	private CacheImage cache;
	private Vector<Wall> walls;
	private Vector<Water> waters;

	public GameBackground(CacheImage _cache) {
		super();
		setOpaque(false);
		bg = 0;
		walls = new Vector<Wall>();
		waters = new Vector<Water>();
		cache = _cache;
		setLayout(new BorderLayout());
	}
	
	public void update(int _bg, Vector<Wall> _walls, Vector<Water> _waters, CacheImage _cache) {
		cache = _cache;
		bg = _bg;
		walls = _walls;
		waters = _waters;
	}
	
	protected void paintComponent(Graphics g) {
		paintBackground(g);
		paintWalls(g);
		paintWaters(g);
	}
	
	public void paintBackground(Graphics g) {
		g.drawImage(cache.getBackground(bg), 0, 0, null);
		g.drawImage(cache.getPanelBackground(), Constants.GameBoardWidth, 0, null);
	}
	
	public void paintWalls(Graphics g) {
		for (Wall wall : walls) {
			Point2D.Double pos = wall.getPosition();
			Point2D.Double ofs = wall.getOffset();
			int x = (int)(pos.getX()-ofs.getX());
			int y = (int)(pos.getY()-ofs.getY());
			Graphics2D g2d = (Graphics2D) g;
			if (Constants.DrawShape)
				g2d.fill(wall.getShape().getCachedArea());
			g.drawImage(cache.getWall(), x, y, null);
			if (Constants.DrawShape)
				g2d.fill(wall.getShape().getCachedArea());
		}
	}
	
	public void paintWaters(Graphics g) {
		for (Water water : waters) {
			Point2D.Double pos = water.getPosition();
			Point2D.Double ofs = water.getOffset();
			int x = (int)(pos.getX()-ofs.getX());
			int y = (int)(pos.getY()-ofs.getY());
			Graphics2D g2d = (Graphics2D) g;
			if (Constants.DrawShape)
				g2d.fill(water.getShape().getCachedArea());
			g.drawImage(cache.getWater(water.getObjectIdx()), x, y, null);
			if (Constants.DrawShape) {
				System.out.println(water.getShape().getCachedArea().getBounds());
				g2d.fill(water.getShape().getCachedArea());
			}
		}
	}
}
