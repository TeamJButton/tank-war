package customerUI;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

import util.Constants;
import util.Util;



public class UserJPanel extends JPanel{
	private static final long serialVersionUID = 3544100696006216948L;
	private Image img;
	public UserJPanel() {
		super();
		setSize(Constants.GameBoardGUIWidth-Constants.GameBoardWidth, Constants.GameBoardHeight);
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		int w = Constants.GameBoardGUIWidth-Constants.GameBoardWidth;
		int h = Constants.GameBoardHeight;
		img = Util.loadImage("background/panel.png").getScaledInstance(w, h, Image.SCALE_DEFAULT);
		g.drawImage(img, 0, 0, null);
		revalidate();
		repaint();
	}
}
