package customerUI;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.util.Vector;

import javax.swing.JPanel;

import client.CacheImage;
import gameObject.Bullet;
import gameObject.GameBoard;
import gameObject.Tank;
import gameObject.Tree;
import server.GameEngine;
import util.Constants;
import util.Util.Transformation;


public class BoardJPanel extends JPanel {
  private static final long serialVersionUID = 5331633849169777211L;
  private GameBoard board;
  private CacheImage cImage;
  private long lastUpdated;

  public BoardJPanel(CacheImage cache) {
    super();
    setLayout(new BorderLayout());
    cImage = cache;
    setOpaque(false);
  }

  protected void paintComponent(Graphics g) {
	lastUpdated = System.currentTimeMillis();
	paintTrees(g);
    paintTanks(g);
    paintBullets(g);
    paintStart(g);
  }
  
  private void paintShadow(Graphics g, String s, int x, int y) {
	  g.setColor(Color.gray);
	  g.drawString(s, x+3, y+3);
	  g.setColor(Color.black);
	  g.drawString(s, x, y);
  }
  
  private void paintStart(Graphics g) {
	  g.setFont(Constants.ArialBlackLarge);
	  int w = g.getFontMetrics().stringWidth("0");
	  if (board.getState() == GameEngine.State.Three) {
		  paintShadow(g, "3", Constants.GameBoardWidth/2-w/2, Constants.GameBoardHeight/2);
	  } else if (board.getState() == GameEngine.State.Two) {
		  paintShadow(g, "2", Constants.GameBoardWidth/2-w/2, Constants.GameBoardHeight/2);
	  } else if (board.getState() == GameEngine.State.One) {
		  paintShadow(g, "1", Constants.GameBoardWidth/2-w/2, Constants.GameBoardHeight/2);
	  }
  }

  private void paintTrees(Graphics g) {
	Vector<Tree> trees = board.getTrees();
    for (Tree tree : trees) {
		if(tree.isDestroyed()){
			continue;
		}
      Point2D.Double pos = tree.getPosition();
      double x = pos.getX()-tree.getOffset().getX();
      double y = pos.getY()-tree.getOffset().getY();
      Image treeImg = cImage.getTree(tree.getObjectIdx());
      Graphics2D g2d = (Graphics2D) g;
      if (Constants.DrawShape)
    	  g2d.fill(tree.getShape().getCachedArea());
      g.drawImage(treeImg, (int) x, (int) y, null);
    }
  }

  private void paintTanks(Graphics g) {
	Vector<Tank> tanks = board.getTanks();
    for (int i = 0; i < tanks.size(); i++) {
      Tank t = tanks.elementAt(i);
      Point2D.Double pos = t.getPosition();
      int x = (int) (pos.getX()-t.getOffset().getX());
      int y = (int) (pos.getY()-t.getOffset().getY());

      if (!t.isAlive())
    	  continue;
      Image objImg = cImage.getTank(t.getObjectIdx(), t.getMyPlayerRecord().color);
      if (!t.isVulnerable()) {
    	  int r = objImg.getHeight(null)/2;
    	  ((Graphics2D) g).setStroke(new BasicStroke(3));
    	  g.drawOval((int)pos.getX()-r, (int)pos.getY()-r, 2*r, 2*r);
      }
      Graphics2D g2d = (Graphics2D) g;
      AffineTransform trans = new AffineTransform();
      if (Constants.DrawShape)
    	  g2d.fill(t.getShape().getCachedArea());
      trans.translate(x, y);
      trans.rotate(Math.toRadians(t.getAngle()), objImg.getWidth(null)/2, objImg.getHeight(null)/2); // -90 because tank is not at 0 degree
      g2d.drawImage(objImg, trans, null);
    }
  }

  private void paintBullets(Graphics g) {
	Vector<Bullet> bullets = board.getBullets();
    for (int i = 0; i < bullets.size(); i++) {    	
      Bullet b = bullets.elementAt(i);
      Point2D.Double pos = b.getPosition();
      double x = pos.getX();
      double y = pos.getY();
      Image objImg = cImage.getBullet();

      // rotate
      Graphics2D g2d = (Graphics2D) g;
      if (Constants.DrawShape)
    	  g2d.fill(b.getShape().getCachedArea());
      AffineTransform trans = new AffineTransform();
      trans.translate(x, y);
      trans.rotate(Math.toRadians(b.getAngle()), objImg.getWidth(null)/2, objImg.getHeight(null)/2);

      g2d.drawImage(objImg, trans, this);
      if (Constants.DrawShape)
    	  g2d.fill(b.getShape().getCachedArea().getBounds());
      
    }
  }
  
  public void setBoard(GameBoard _board) {
	  board = _board;
  }

  public void localUpdate() {
	  long newTime = System.currentTimeMillis();
	  if (newTime > lastUpdated + 3) {
		  double dtime = (double)(newTime-lastUpdated)/1000.0;
		  lastUpdated = newTime;
		  Vector<Tank> tanks = board.getTanks();
		  for (Tank tank : tanks) {
			  if (tank.lastTrans != Transformation.Still)
				  tank.updatePosition(dtime, tank.lastTrans);
		  }
		  Vector<Bullet> bullets = board.getBullets();
		  for (Bullet bullet : bullets) {
			  if (bullet.lastTrans != Transformation.Still)
				  bullet.updatePosition(dtime, bullet.lastTrans);
		  }
	  }
	  revalidate();
	  repaint();
  }
}
