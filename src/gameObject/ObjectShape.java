package gameObject;

import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Point2D.Double;
import java.io.Serializable;
import java.util.Vector;

public class ObjectShape implements Serializable {
	private static final long serialVersionUID = -3032074820438209355L;
	transient private Area area;
	transient private Area cacheArea;
	private boolean changed;
	/*transient*/ private Vector<Point2D.Double> silh;
	private Point2D.Double pos;
	private double rotAngle; 
	
	public ObjectShape(Vector<Point2D.Double> pts, Point2D.Double pos) {
		silh = pts;
		this.recalculateArea();
		this.pos = pos;
		rotAngle = 0;
		changed = true;
	}
	
	public ObjectShape() {
		silh = new Vector<Point2D.Double>();
		pos = new Point2D.Double(0, 0);
		rotAngle = 0;
		changed = true;
	}
	
	@SuppressWarnings("unchecked")
	public ObjectShape(ObjectShape other) {
		this.silh = (Vector<Point2D.Double>) other.silh.clone();
		this.area = (Area) other.area.clone();
		this.cacheArea = (Area) other.cacheArea.clone();
		this.pos = (Double) other.pos.clone();
		this.rotAngle = other.rotAngle;
		this.changed = other.changed;
	}
	
	public void addPoint(Point2D.Double point) {
		silh.add(point);
		recalculateArea();
	}
	
	private void recalculateArea() {
		Path2D.Double path = new Path2D.Double();
		path.moveTo(silh.get(0).getX(), silh.get(0).getY());
		for (int i = 1; i < silh.size(); ++i) {
			path.lineTo(silh.get(i).getX(), silh.get(i).getY());
		}
		path.closePath();
		area = new Area(path);
		cacheArea = (Area) area.clone();
	}
	
	public Area getCachedArea() {
		updateCache();
		return cacheArea;
	}
	
	public void translate(double delx, double dely) {
		pos.setLocation(pos.getX() + delx, pos.getY() + dely);
		changed = true;
	}
	
	public void rotate(double angle) {
		rotAngle = (rotAngle + angle) % 360;
		changed = true;
	}
	
	public void updateCache() {
		if (area == null) {
			recalculateArea();
		}
		if (changed) {
			AffineTransform tr1 = AffineTransform.getRotateInstance(rotAngle * Math.PI / 180);
			cacheArea = this.area.createTransformedArea(tr1);
			AffineTransform tr2 = AffineTransform.getTranslateInstance(pos.getX(), pos.getY());
			cacheArea = this.cacheArea.createTransformedArea(tr2);
			changed = false;
		} 
	}
	
	public boolean collidesWith(ObjectShape otherShape) {
		this.updateCache();
		otherShape.updateCache();
		Area areaClone = (Area) this.cacheArea.clone();
		areaClone.intersect(otherShape.getCachedArea());
		return !areaClone.isEmpty();
	}
	
	public void setAngle(double angle) {
		rotAngle = angle;
	}
	
	public double getAngle() {
		return rotAngle;
	}
	
	public Point2D.Double getPosition() {
		return pos;
	}
	
	public void setPosition(Point2D.Double _pos) {
		pos = _pos;
		changed = true;
	}
	
	public void setPosition(double x, double y) {
		pos.x = x;
		pos.y = y;
	}
	
	public boolean touchBoundary() {
		return false; //TODO
	}
}
