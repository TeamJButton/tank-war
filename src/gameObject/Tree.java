package gameObject;

import java.awt.geom.Point2D;
import java.util.Vector;

import util.Constants;

public class Tree extends GameObject {
	private static final long serialVersionUID = 5622351504968708658L;
	
	private static final double[][][] TreeSilh =
		{
			{{0.1, 0.9}, {-0.37, 0.74}, {-0.47, 0.36}, {-0.81, 0.34}, {-0.66, -0.23},
				{-0.4, 0.77}, {-0.05, -0.65}, {0.41, -0.73}, {0.49, -0.39}, {0.83, -0.19},
				{0.54, 0.12}, {0.66, 0.57}, {0.37, 0.62}},
			{{0.19, 0.78}, {-0.33, 0.66}, {-0.72, 0.13}, {-0.69, -0.39},
				{-0.1, -0.79}, {0.41, -0.72}, {0.75, -0.18}, {0.62, 0.3}},
			{{0.48, 0.61}, {-0.37, 0.65}, {-0.57, 0.4}, {-0.78, 0.1},
				{-0.57, -0.14}, {-0.62, -0.5}, {-0.08, -0.52}, {0.2, -0.88}, {0.76, -0.15}},
			{{0.45, 0.86}, {-0.14, 0.96}, {-0.77, 0.42}, {-0.47, -0.37},
					{0, -0.5}, {0.5, -0.5}, {0.84, -0.05}, {0.79, 0.45}},
			{{0, 0.9}, {-0.51, 0.66}, {-0.86, 0.08}, {-0.61, -0.5},
					{-0.02, -0.85}, {0.5, -0.66}, {0.86, 0}, {0.57, 0.61}}
		};
	public static final double[][] TreeSize =
		{
			{80, 80}, {60, 60}, {90, 90}, {70, 70}, {90, 90}
		};
	
	public Tree(Point2D.Double pos) {
		super(pos);
		reflectBullet = false;
		passBullet = false;
		objectIdx = Constants.getRandomTree();
		drawShape(pos);
	}
	
	private void drawShape(Point2D.Double pos) {
		Vector<Point2D.Double> pts = new Vector<Point2D.Double>();
		for (int i = 0; i < TreeSilh[objectIdx].length; ++i) {
			pts.add(new Point2D.Double(TreeSilh[objectIdx][i][0]*TreeSize[objectIdx][0]/2,
					TreeSilh[objectIdx][i][1]*TreeSize[objectIdx][1]/2));
		}
		shape = new ObjectShape(pts, pos);
		offset = new Point2D.Double(TreeSize[objectIdx][0]/2, TreeSize[objectIdx][1]/2);
	}

	@Override
	public boolean canCollide() {
		return true;
	}
}
