package gameObject;

import java.awt.geom.Point2D;
import java.io.Serializable;

import util.Util;
import util.Util.Transformation;

public abstract class MovingObject extends GameObject implements Serializable {
	private static final long serialVersionUID = 875635414679100426L;
	protected double speed;
	public Transformation lastTrans;
	
	public MovingObject(Point2D.Double pos) {
		super(pos);
		lastTrans = Transformation.Still;
	}
	
	public void setShape(ObjectShape shape) {
		this.shape = shape;
	}
	
	public void setAngle(double angle) {
		this.shape.setAngle(angle);;
	}
	
	public double getAngle() {
		return this.shape.getAngle();
	}
	
	public void updatePosition(double time, Transformation transType) {
		double dist = 0;
		double rotAngle = 0;
		lastTrans = transType;
		switch (transType) {
		case Forward: 
			dist = time * speed;
			break;
		case Backward:
			dist = -1 * time * speed;
			break;
		case Right: 
			rotAngle = time * Util.angSpd;
			break;
		case Left: 
			rotAngle = -time * Util.angSpd;
			break;
		case ForwardRight:
			dist = time * speed;
			rotAngle = time * Util.angSpd;
			break;
		case ForwardLeft:
			dist = time * speed;
			rotAngle = -time * Util.angSpd;
			break;
		case BackwardRight:
			dist = -1 * time * speed;
			rotAngle = -time * Util.angSpd;
			break;
		case BackwardLeft:
			dist = -1 * time * speed;
			rotAngle = time * Util.angSpd;
			break;
		default:
			return;
		}
		double nowx = shape.getPosition().x;
		double nowy = shape.getPosition().y;
		shape.rotate(rotAngle);
		double x = dist * Math.sin(shape.getAngle() * Math.PI / 180);
		double y = -dist * Math.cos(shape.getAngle() * Math.PI / 180);	
		shape.setPosition(x + nowx, y + nowy);
	}

	//Needs testing
	public ObjectShape shapeAfter(double time, Util.Transformation transType) {
		ObjectShape copyShape = new ObjectShape(shape);
		double dist = 0;
		double rotAngle = 0;
		switch (transType) {
		case Forward: 
			dist = time * speed;
			break;
		case Backward:
			dist = -1 * time * speed;
			break;
		case Right: 
			rotAngle = time * Util.angSpd;
			break;
		case Left: 
			rotAngle = -time * Util.angSpd;
			break;
		case ForwardRight:
			dist = time * speed;
			rotAngle = time * Util.angSpd;
			break;
		case ForwardLeft:
			dist = time * speed;
			rotAngle = -time * Util.angSpd;
			break;
		case BackwardRight:
			dist = -1 * time * speed;
			rotAngle = -time * Util.angSpd;
			break;
		case BackwardLeft:
			dist = -1 * time * speed;
			rotAngle = time * Util.angSpd;
			break;
		default:
			break;
		}
		double x = dist * Math.sin((rotAngle + shape.getAngle()) * Math.PI / 180);
		double y = -dist * Math.cos((rotAngle + shape.getAngle()) * Math.PI / 180);
		copyShape.translate(x, y);
		copyShape.rotate(rotAngle);
		return copyShape;
	}
}
