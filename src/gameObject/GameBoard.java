package gameObject;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.Vector;

import server.GameEngine;
import server.GameRoom;
import server.ServerClientCommunicator;
import util.Constants;

public class GameBoard implements Serializable {
  private static final long serialVersionUID = 978935505994821378L;
  private GameRoom room;
  private volatile Vector<Tree> trees;
  private volatile Vector<Water> waters;
  private volatile Vector<Wall> walls;
  private volatile Vector<Tank> tanks;
  private volatile Vector<Bullet> bullets;
  private transient Vector<ObjectShape> boundaries;
  private int bg;
  private boolean gameOver;
  private long time;
  private GameEngine.State state;
  public boolean shootSound;
  public boolean hitTankSound;
  public boolean hitTreeSound;
  public boolean hitWallSound;
  public boolean beepSound;
  public boolean startSound;

  public GameBoard() {
	trees = new Vector<Tree>();
	waters = new Vector<Water>();
	walls = new Vector<Wall>();
	tanks = new Vector<Tank>();
	bullets = new Vector<Bullet>();
  }
  
  public GameBoard(GameRoom _room) {
	trees = new Vector<Tree>();
	waters = new Vector<Water>();
	walls = new Vector<Wall>();
    tanks = new Vector<Tank>();
    bullets = new Vector<Bullet>();
    boundaries = new Vector<ObjectShape>();
    room = _room;
    bg = Constants.getRandomBackground();
    boolean occupied[][] = new boolean[10][10];
    for (int y = 0; y < 10; ++y) {
      for (int x = 0; x < 10; ++x) {
        occupied[x][y] = (y < 3 || y > 6) && (x < 3 || x > 6);
      }
    }
    initTanks(occupied);
    initObstacles(occupied);

    gameOver = false;
  }

  private Point2D.Double scanFlipPosition(boolean occupied[][], Integer a, Integer b) {
    boolean flag = false;
    int gridx = 0, gridy = 0;
    while (!flag) {
      gridx = Constants.getRandomInt(10);
      gridy = Constants.getRandomInt(10);
      a = gridx;
      b = gridy;
      if (!occupied[gridx][gridy]) {
        occupied[gridx][gridy] = true;
        flag = true;
      }
    }
    int gridw = Constants.GameBoardWidth / 10;
    int gridh = Constants.GameBoardHeight / 10;
    int dx = gridw/2; //Constants.getRandomInt(gridw);
    int dy = gridh/2; //Constants.getRandomInt(gridh);
    Point2D.Double pos = new Point2D.Double(gridx * gridw + dx, gridy * gridh + dy);
    return pos;
  }

  private void initObstacles(boolean occupied[][]) {
    if (tanks == null) {
      System.out.println("Tanks not initialized yet");
      return;
    }
    trees.clear();
    walls.clear();
    waters.clear();
    boundaries.clear();
    int treeCount = Constants.getRandomInt(Constants.MaxTreeCount - Constants.MinTreeCount)
        + Constants.MinTreeCount;
    for (int i = 0; i < treeCount; i++) {
      boolean fail = true;
      while (fail) {
        fail = false;
        Integer x = 0;
        Integer y = 0;
        Tree tree = new Tree(scanFlipPosition(occupied, x, y));
        for (Tank t : tanks) {
          if (tree.getShape().collidesWith(t.getShape())) {
            fail = true;
            occupied[x][y] = false;
            break;
          }
        }
        if (!fail) {
          trees.addElement(tree);
        }
      }
    }
    int wallCount = Constants.getRandomInt(Constants.MaxWallCount - Constants.MinWallCount)
        + Constants.MinWallCount;
    for (int i = 0; i < wallCount; i++) {
      boolean fail = true;
      while (fail) {
        fail = false;
        Integer x = 0;
        Integer y = 0;
        Wall wall = new Wall(scanFlipPosition(occupied, x, y));
        for (Tank t : tanks) {
          if (wall.getShape().collidesWith(t.getShape())) {
            fail = true;
            occupied[x][y] = false;
            break;
          }
        }
        if (!fail) {
          walls.addElement(wall);
        }
      }
    }
    int waterCount = Constants.getRandomInt(Constants.MaxWaterCount - Constants.MinWaterCount)
        + Constants.MinWaterCount;
    for (int i = 0; i < waterCount; i++) {
      boolean fail = true;
      while (fail) {
        fail = false;
        Integer x = 0;
        Integer y = 0;
        Water water = new Water(scanFlipPosition(occupied, x , y));
        for (Tank t : tanks) {
          if (water.getShape().collidesWith(t.getShape())) {
            fail = true;
            occupied[x][y] = false;
            break;
          }
        }
        if (!fail) {
          waters.addElement(water);
        }
      }
    }
    int ofs = 20;
	int boundaryPoints[][][] = {
	//top
	  {{0,0}, {Constants.GameBoardWidth, 0},
		{Constants.GameBoardWidth, -ofs}, {0, -ofs}},
	//bottom
	  {{0,Constants.GameBoardHeight}, {Constants.GameBoardWidth, Constants.GameBoardHeight},
		{Constants.GameBoardWidth, Constants.GameBoardHeight+ofs}, {0, Constants.GameBoardHeight+ofs}},
	//left
	  {{0,0}, {-ofs, 0},
		{-ofs, Constants.GameBoardHeight}, {0, Constants.GameBoardHeight}},
	//right
	  {{Constants.GameBoardWidth+ofs, 0}, {Constants.GameBoardWidth, 0},
			{Constants.GameBoardWidth, Constants.GameBoardHeight}, {Constants.GameBoardWidth+ofs, Constants.GameBoardHeight}}
	};
    for (int i = 0; i < 4; i++) {
	  ObjectShape bound = new ObjectShape();
	  for (int j = 0; j < 4; j++) {
		bound.addPoint(new Point2D.Double(boundaryPoints[i][j][0], boundaryPoints[i][j][1]));
	  }
	  boundaries.add(bound);
    }
  }

  private void initTanks(boolean occupied[][]) {
    tanks.clear();
    Vector<ServerClientCommunicator> players = room.players;
    for (int i = 0; i < players.size(); ++i) {
      int id = players.get(i).getPlayerRecord().id;
      System.out.println(players.get(i).getName() + "Game Board ID: " + id);
      double x = Constants.startingPoints[i].getX();
      double y = Constants.startingPoints[i].getY();
      Tank tank = new Tank(players.get(i).getPlayerRecord(), new Point2D.Double(x, y), id);
      System.out.println("New tanks generated"); //TODO delete
      tank.setAngle(Constants.startingOrientations[i]);
      tanks.add(tank);
    }
  }

  public int getBackgroundInt() {
    return bg;
  }

  public void setBackground(int bg) {
    this.bg = bg;
  }

  public Vector<Tree> getTrees() {
    return trees;
  }
  
  public Vector<Water> getWaters() {
	  return waters;
  }
  
  public Vector<Wall> getWalls() {
	  return walls;
  }

  public Vector<Tank> getTanks() {
    return tanks;
  }

  public Vector<Bullet> getBullets() {
    return bullets;
  }
  
  public Vector<ObjectShape> getBoundaries() {
	  return boundaries;
  }

  public void addTank(Tank tank) {
    tanks.add(tank);
  }

  public void addBullet(Bullet bullet) {
    bullets.add(bullet);
  }
  
  public void addWall(Wall wl) {
	  walls.add(wl);
  }
  
  public void addWater(Water wr) {
	  waters.add(wr);
  }

  public void addTree(Tree tr) {
	  trees.add(tr);
  }
  
  public void removeTank(Tank tank) {
	  tanks.remove(tank);
  }
  
  public void setBullets(Vector<Bullet> _bullets) {
	  bullets = _bullets;
  }
  
  public void removeTree(Tree tree) {
	  trees.remove(tree);
  }

  public void finish() {
    gameOver = true;
  }

  public boolean isOver() {
    return gameOver;
  }

  public void setTime(long time) {
    this.time = time;
  }

  public long getTime() {
    return time;
  }
  
  public GameEngine.State getState() {
	  return state;
  }
  
  public void setState(GameEngine.State _state) {
	  state = _state;
  }
  
  public void resetSounds() {
	  shootSound = hitTankSound = hitTreeSound = hitWallSound = beepSound = startSound = false;
  }
}
