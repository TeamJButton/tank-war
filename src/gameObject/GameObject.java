package gameObject;

import java.awt.geom.Point2D;
import java.io.Serializable;

public abstract class GameObject implements Serializable {

	private static final long serialVersionUID = -3576924100854192213L;
	protected int objectIdx;
	protected ObjectShape shape;
	protected boolean reflectBullet;
	protected boolean passBullet;
	protected boolean destroyed;
	protected Point2D.Double offset;
	
	public GameObject(Point2D.Double pos) {
		this.destroyed = false;
	}
	
	public abstract boolean canCollide();
	
	public Point2D.Double getPosition() {
		return shape.getPosition();
	}
	
	public ObjectShape getShape() {
		return shape;
	}
	
	public boolean canReflectBullet() {
		return reflectBullet;
	}
	
	public boolean canPassBullet() {
		return passBullet;
	}
	
	public void destroy() {
		destroyed = true;
	}
	
	public boolean isDestroyed() {
		return destroyed;
	}
	
	public int getObjectIdx() {
		return objectIdx;
	}
	
	public void setObjectIdx(int idx) {
		this.objectIdx = idx;
	}
	
	public Point2D.Double getOffset() {
		return offset;
	}

	public void setOffset(Point2D.Double offset) {
		this.offset = offset;
	}
}
