package gameObject;

import java.awt.geom.Point2D;
import java.util.Vector;

import util.Constants;

public class Wall extends GameObject {
	private static final long serialVersionUID = -4507484091742591346L;
	private static final double WallSize[][] =
		{
				{Constants.WallLength, Constants.WallLength}
		};
	
	public Wall(Point2D.Double pos) {
		super(pos);
		reflectBullet = true;
		passBullet = false;
		objectIdx = Constants.getRandomWall();
		drawShape(pos);
	}
	
	private void drawShape(Point2D.Double position) {
		Vector<Point2D.Double> pts = new Vector<Point2D.Double>();
		double w = WallSize[objectIdx][0], h = WallSize[objectIdx][1];
		pts.add(new Point2D.Double(w/2, h/2));
		pts.add(new Point2D.Double(-w/2, h/2));
		pts.add(new Point2D.Double(-w/2, -h/2));
		pts.add(new Point2D.Double(w/2, -h/2));
		shape = new ObjectShape(pts, position);
		offset = new Point2D.Double(w/2, h/2);
	}

	@Override
	public boolean canCollide() {
		return true;
	}
}
