package gameObject;

import java.awt.geom.Point2D;
import java.util.Vector;

import server.PlayerRecord;
import util.Constants;
import util.Util;

public class Tank extends MovingObject {
	private static final long serialVersionUID = 7107045191309760819L;
	private int id;
	private int kills;
	private int deaths;
	private long reload;
	private long respawnTimerL;
	private long invincibleTimerL;
	private PlayerRecord myPlayerRecord;
	private boolean needRevive;
	public static final double[] TankWidths = {71, 71, 71, 86, 72, 94, 94, 94, 126};
	public static final double[] TankHeights = {128, 128, 128, 128, 128, 128, 128, 128, 128};
	public static final double[] TankScale = {0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5};
	private int rank;
	private transient final int cannon = 3;
	private transient final int body = 25;
	
	public Tank(PlayerRecord data, Point2D.Double pos, int id) {
		super(pos);
		myPlayerRecord = data;
		reflectBullet = false;
		passBullet = false;
		this.id = id;
		this.speed = Util.tankSpd;
		this.rank = 3; //Default minimum experience gain
		kills = 0;
		deaths = 0;
		reload = 0;
		respawnTimerL = 0;
		invincibleTimerL = 0;
		needRevive = false;
		
		objectIdx = util.Util.levelFromScore(data.score)-1;
		drawShape(data, pos);
	}
	
	private void drawShape(PlayerRecord pr, Point2D.Double pos) {
		int level = util.Util.levelFromScore(pr.score)-1;
		Vector<Point2D.Double> pts = new Vector<Point2D.Double>();
		double w = TankWidths[level]*TankScale[level];
		double h = TankHeights[level]*TankScale[level];
		pts.add(new Point2D.Double(w/2, h/2));
		pts.add(new Point2D.Double(-w/2, h/2));
		pts.add(new Point2D.Double(-w/2, -h/2+body));
		pts.add(new Point2D.Double(-cannon, -h/2+body));
		pts.add(new Point2D.Double(-cannon, -h/2));
		pts.add(new Point2D.Double(cannon, -h/2));
		pts.add(new Point2D.Double(cannon, -h/2+body));
		pts.add(new Point2D.Double(w/2, -h/2+body));
		shape = new ObjectShape(pts, pos);
		offset = new Point2D.Double(w/2, h/2);
	}
	
	public PlayerRecord getMyPlayerRecord() {
		return myPlayerRecord;
	}

	public void setRank(int rank) {
		if (rank < 4 && rank >= 0) {
			this.rank = rank;
		}
	}
	
	public int getRank() {
		return this.rank;
	}
	
	public long getReload() {
		return this.reload;
	}
	
	public void setReload() {
		this.reload = Constants.ReloadTime;
	}
	
	public void decrementReload(long time) {
		this.reload -= time;
		if (this.reload < 0) {
			this.reload = 0;
		}
	}
	
	public void incKill() {
		kills++;
	}
	
	public void incDeath() {
		deaths++;
	}
	
	public int getKills() {
		return kills;
	}
	
	public int getDeaths() {
		return deaths;
	}
	
	public int getID() {
		return id;
	}
	
	public void setRespawn() {
		respawnTimerL = Constants.RespawnDuration;
	}
	
	public void setInvincible() {
		invincibleTimerL = Constants.InvincibilityDuration;
	}
	
	public void decrementRespawn(long time) {
		respawnTimerL = respawnTimerL - time;
		if (respawnTimerL < 0) {
			respawnTimerL = 0;
			needRevive = true;
		}
	}
	
	public boolean needRevive() {
		if (needRevive) {
			needRevive = false;
			return true;
		} else {
			return false;
		}
	}
	
	public void decrementInvincibility(long time) {
		invincibleTimerL = invincibleTimerL - time;
		if (invincibleTimerL < 0) {
			invincibleTimerL = 0;
		}
	}
	
	public Bullet fireBullet() {
		double theta = this.getAngle();
		Point2D.Double bulletPos=new Point2D.Double(this.getPosition().getX(), this.getPosition().getX());
		double x = this.getPosition().getX()+50*Math.cos((theta-90)*Math.PI/180);
		double y = this.getPosition().getY()+50*Math.sin((theta-90)*Math.PI/180);
		bulletPos.setLocation(x, y);
		return new Bullet(theta, bulletPos, id);
	}
	
	public boolean isAlive() {
		return (respawnTimerL == 0 && !needRevive);
	}
	
	public boolean isVulnerable() {
		return respawnTimerL == 0 && invincibleTimerL == 0;
	}
	
	@Override
	public boolean canCollide() {
		return true;
	}
	
	public int getKillDeath() {
		return kills - deaths;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public void setReload(long reload) {
		this.reload = reload;
	}
}
