package gameObject;

import java.awt.geom.Point2D;
import java.util.Vector;

import util.Constants;

public class Water extends GameObject {
	private static final long serialVersionUID = 1459505162676646358L;
	private static final double[][][] WaterSilh =
		{
			{{0.87, 0.69}, {-0.57, 0.75}, {-1.01, 0.3}, {-0.82, -0.22}, {-0.58, -0.89}, {0.02, -0.92}, {0.24, -0.29}, {0.99, 0.31}},
			{{0.4, 1.01}, {-0.31, 1.09}, {-0.97, 0.42}, {-0.79, -0.51},
				{-0.14, -0.92}, {0.56, -0.93}, {1, -0.5}, {0.84, 0.61}},
			{{-0.4, 1}, {-0.95, 0.6}, {-0.9, 0.07}, {-1, -0.5},
				{-0.78, -0.8}, {0.5, -1}, {0.89, -0.79}, {0.99, -0.27}, {0.89, 0.57}, {0.64, 0.85}}
		};
	public static final double[][] WaterSize =
		{
			{60, 60}, {60, 60}, {40, 60}
		};
	
	public Water(Point2D.Double pos) {
		super(pos);
		reflectBullet = false;
		passBullet = true;
		objectIdx = Constants.getRandomWater();
		drawShape(pos);
	}
	
	private void drawShape(Point2D.Double pos) {
		Vector<Point2D.Double> pts = new Vector<Point2D.Double>();
		for (int i = 0; i < WaterSilh[objectIdx].length; ++i) {
			pts.add(new Point2D.Double(WaterSilh[objectIdx][i][0]*WaterSize[objectIdx][0]/2,
					WaterSilh[objectIdx][i][1]*WaterSize[objectIdx][1]/2));
		}
		shape = new ObjectShape(pts, pos);
		offset = new Point2D.Double(WaterSize[objectIdx][0]/2, WaterSize[objectIdx][1]/2);
	}

	@Override
	public boolean canCollide() {
		return false;
	}

}
