package gameObject;

import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.Vector;

import util.Constants;
import util.Util;

public class Bullet extends MovingObject implements Serializable {
  private static final long serialVersionUID = 757112432436194238L;
  transient private int id;
  transient private int bounce;
  private static final double Silh[][] =//59.5 107.5
      {{Constants.BulletWidth/2, Constants.BulletHeight/2}, {-Constants.BulletWidth/2, Constants.BulletHeight/2},
    		  {-Constants.BulletWidth/2, -Constants.BulletHeight/2}, {Constants.BulletWidth/2, -Constants.BulletHeight/2}};
  private double BulletOffset[] = {Constants.BulletWidth/2, Constants.BulletHeight/2};

  public Bullet(double angle, Point2D.Double pos, int id) {
    super(pos);
    passBullet = true;
    reflectBullet = false;
    this.id = id;
    this.speed = Util.bulletSpd;
    bounce = 3;
    drawShape(pos);
    shape.setAngle(angle);
  }

  public void drawShape(Point2D.Double position) {
    Vector<Point2D.Double> pts = new Vector<Point2D.Double>();
    for (int i = 0; i < Silh.length; ++i) {
      pts.add(new Point2D.Double(Silh[i][0], Silh[i][1]));
    }
    shape = new ObjectShape(pts, position);
    offset = new Point2D.Double(BulletOffset[0], BulletOffset[1]);
  }

  public int getID() {
    return id;
  }

  public boolean decrementBounce() {
    if (bounce <= 0) {
      return false;
    }
    bounce--;
    return true;
  }

  // TODO: Wrong
  public void reflect(Point2D.Double incident) {
	double oldtheta = shape.getAngle()*Math.PI/180;
	double ux = Math.sin(oldtheta);
	double uy = Math.cos(oldtheta);
	double dot = ux*incident.getX() + uy*incident.getY();
	double v2 = incident.getX()*incident.getX() + incident.getY()*incident.getY();
	double vx = ux - 2*dot*incident.getX()/v2;
	double vy = uy - 2*dot*incident.getY()/v2;
	double theta = (Math.PI/2-Math.atan2(vy, vx))/Math.PI*180;
	if (theta < 0)
		theta += 360;
	shape.setAngle(theta);
  }
  
  public boolean collideBound(ObjectShape bound, double incx, double incy) {
	  if (destroyed)
		  return false;
	  if (shape.collidesWith(bound)) {
		  if (!decrementBounce()) {
			  this.destroy();
			  return false;
		  } else {
			  reflect(new Point2D.Double(incx, incy));
		  }
		  return true;
	  }
	  return false;
  }

  public boolean BulletCollide(GameObject obj, Vector<Tank> tanks) {
	if (this.destroyed){
		return false;
	}
	if (!this.shape.collidesWith(obj.shape)) {
		return false;
	}
    if (obj instanceof Tank) {
	  this.destroy();
	  Tank curTank = (Tank) obj;
	  if(!curTank.isVulnerable()){ 
		  return true;
	  }

	  curTank.setRespawn();
	  curTank.incDeath();
  
	  int killer = this.getID();
	  if (killer == curTank.getID()) { 
	    curTank.incDeath(); //Double penalty for suicide
	    return true; 
	  }
	  for (Tank t : tanks) {
	    if (t.getID() == killer) {
	      t.incKill();
	    }
	  }
    } else if (obj instanceof Wall) {//can reflect
    	if (!this.decrementBounce()){//already bounced 3 times
    		this.destroy();
    		return false;
    	} else{
    		double posX = getPosition().getX() - obj.getPosition().getX();
    		double posY = obj.getPosition().getY() - getPosition().getY();
    		//System.out.println("pos x pos y"+posX+" "+posY);
    		double angle = Math.atan2(posY, posX);
    		if (angle < 0)
    			angle += 2*Math.PI;
    		if ((angle > Math.PI/4 && angle < Math.PI*3/4) ||
    			(angle > Math.PI*5/4 && angle < Math.PI*7/4))
    			reflect(new Point2D.Double(0, 1));
    		else
    			reflect(new Point2D.Double(1, 0));
  		  	System.out.println("Wall hit");
    	}
    } else if(obj instanceof Tree){
    	if(obj.destroyed) return false;
    	System.out.println("Tree hit");
    	obj.destroy();
    	this.destroy();
    }
    return true;
  }
  
  @Override
  public boolean canCollide() {
    return true;
  }
}
