package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class RoomCreatorGUI extends JDialog {
  private static final long serialVersionUID = 1L;
  private JLabel jlFail;
  private JLabel jlRoom;
  private JTextField jtfRoom;
  private JButton jbCreate;
  private Client client;
  private RoomCreatorGUI self;

  public RoomCreatorGUI(JFrame owner, Client _client) {
    super(owner, "Create New Room", true);
    self = this;
    this.client = _client;
    // TODO: Window Initialization
    setSize(400, 250);
    setMinimumSize(new Dimension(400, 250));
    setLocationRelativeTo(null); // Center on the screen
    setDefaultCloseOperation(DISPOSE_ON_CLOSE); // Exit Action
    jlFail = new JLabel(" ");
    jlFail.setForeground(Color.red);
    jlFail.setFont(new Font("Arial", Font.PLAIN, 18));
    jlRoom = new JLabel("Room Name: (Max 15 characters)");
    jlRoom.setFont(new Font("Arial", Font.PLAIN, 18));
    jtfRoom = new JTextField();
    jtfRoom.setDocument(new LengthRestriction(15));
    jtfRoom.setHorizontalAlignment(JTextField.CENTER);
    jtfRoom.setFont(new Font("Arial", Font.PLAIN, 18));
    jbCreate = new JButton("Create");
    jbCreate.setFont(new Font("Arial", Font.PLAIN, 18));
    jtfRoom.setMinimumSize(new Dimension(250, 28));
    jtfRoom.setPreferredSize(new Dimension(250, 28));
    jtfRoom.setMaximumSize(new Dimension(250, 28));
    jbCreate.setMinimumSize(new Dimension(200, 28));
    jbCreate.setPreferredSize(new Dimension(200, 28));
    jbCreate.setMaximumSize(new Dimension(200, 28));
    setLayout(new GridBagLayout());
    GridBagConstraints gbc = new GridBagConstraints();
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.insets = new Insets(10, 0, 10, 0);
    add(jlFail, gbc);
    gbc.gridy = 1;
    add(jlRoom, gbc);
    gbc.gridy = 2;
    add(jtfRoom, gbc);
    gbc.gridy = 3;
    add(jbCreate, gbc);
    jbCreate.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
    	if (jtfRoom.getText().trim().length() == 0) {
    		jlFail.setText("Room name cannot be empty.");
    	} else {
            jlFail.setText(" ");
            client.createGameRoom(jtfRoom.getText());
            self.dispose();
    	}
      }
    });
    setVisible(true);
  }

  void CreateFail() {
    jlFail.setText("Room Already Exist");
  }
}


class LengthRestriction extends PlainDocument {
  private static final long serialVersionUID = 1L;
  private final int limit;

  public LengthRestriction(int limit) {
    this.limit = limit;
  }

  @Override
  public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
    if (str == null)
      return;

    if ((getLength() + str.length()) <= limit) {
      super.insertString(offs, str, a);
    }
  }
}
