package client;

import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import customerUI.UserJPanel;
import gameObject.Tank;
import util.Constants;

public class GameOverGUI extends JFrame {
	private static final long serialVersionUID = 2826400343173426796L;
	private Vector<Tank> tanks;
	public GameOverGUI() {
		super("Game Over");
	    setSize(1000,400);
		setLocation(100, 100);// horizontal,vertical
		setFocusable(true);
	  }
	  
	public void updateResults(Vector<Tank> _tanks) {
		tanks = _tanks;
		refresh();
		revalidate();
		repaint();
	}
	  
	private void refresh() {
		getContentPane().removeAll();
		setSize(230*tanks.size(), 400);
		setLayout(new GridLayout(0,tanks.size()));
		 for (int i = 0; i < tanks.size(); i++) {
		    	Tank currTank = tanks.get(i);
			    UserJPanel thisPlayer = new UserJPanel();
			    thisPlayer.setLayout(new BoxLayout(thisPlayer, BoxLayout.Y_AXIS));
			
			      JLabel name = new JLabel(" USER NAME: ");
			      JLabel username=new JLabel(currTank.getMyPlayerRecord().name);
			      JLabel killLabel = new JLabel("Kill: ");
			      JLabel kill=new JLabel(Integer.toString(currTank.getKills()));
			      JLabel deathLabel = new JLabel("Death: ");
			      JLabel death=new JLabel(Integer.toString(currTank.getDeaths()));
			      JLabel scoreLabel = new JLabel("Score: ");
			      JLabel score = new JLabel("" + (Constants.rankScore[currTank.getRank()]));
			      JLabel experienceLabel = new JLabel("Experience: ");
			      JLabel experience = new JLabel(currTank.getMyPlayerRecord().score % 1000 + "/1000"); 
			      JLabel levelLabel = new JLabel("Level: ");
			      JLabel level = new JLabel(
			          Integer.toString(util.Util.levelFromScore(currTank.getMyPlayerRecord().score))); 
			      
			      name.setFont(Constants.ArialItalic);
			      username.setFont(Constants.ArialItalic);
			      killLabel.setFont(Constants.ArialPlain);
			      kill.setFont(Constants.ArialItalic);
			      deathLabel.setFont(Constants.ArialPlain);
			      death.setFont(Constants.ArialItalic);
			      scoreLabel.setFont(Constants.ArialPlain);
			      score.setFont(Constants.ArialItalic);
			      experienceLabel.setFont(Constants.ArialPlain);
			      experience.setFont(Constants.ArialItalic);
			      levelLabel.setFont(Constants.ArialPlain);
			      level.setFont(Constants.ArialItalic);
			      
			      
			      thisPlayer.add(name);
			      thisPlayer.add(username);
			      thisPlayer.add(killLabel);
			      thisPlayer.add(kill);
			      thisPlayer.add(deathLabel);
			      thisPlayer.add(death);
			      thisPlayer.add(scoreLabel);
			      thisPlayer.add(score);
			      thisPlayer.add(experienceLabel);
			      thisPlayer.add(experience);
			      thisPlayer.add(levelLabel);
			      thisPlayer.add(level);
			      
			      thisPlayer.setBorder(new LineBorder(null));
			      add(thisPlayer);
			  }
	}
}
