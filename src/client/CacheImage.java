package client;

import java.awt.Image;
import java.util.Vector;

import gameObject.Tank;
import gameObject.TankColor;
import gameObject.Tree;
import gameObject.Water;
import util.Constants;
import util.Util;

public class CacheImage {
	
	private Image bullet;
	private Vector<Image> tank;
	private Vector<Image> tree;
	private Image wall;
	private Vector<Image> water;
	private Vector<Image> backgrounds;
	private Image panelBackground;
	
	
	public CacheImage() {
		bullet = Util.loadImage("bullet/sbullet.png").getScaledInstance(Constants.BulletWidth, Constants.BulletHeight, Image.SCALE_DEFAULT);
		tree = new Vector<Image>();
		for (int i=1; i<=5; i++) {
			tree.add(Util.loadImage("tree/tree" + i + ".png").getScaledInstance((int)Tree.TreeSize[i-1][0], (int)Tree.TreeSize[i-1][1], Image.SCALE_DEFAULT));
		}
		wall = Util.loadImage("wall/wall1.png").getScaledInstance(Constants.WallLength, Constants.WallLength, Image.SCALE_DEFAULT);
		water = new Vector<Image>();
		for (int i=1; i<=3; i++) {
			water.add(Util.loadImage("water/water" + i + ".png").getScaledInstance((int)Water.WaterSize[i-1][0], (int)Water.WaterSize[i-1][1], Image.SCALE_DEFAULT));
		}
		tank = new Vector<Image>();
		for (int i=1; i<=Constants.MaxLevel; i++) {
			//blue, green, red
			tank.add(Constants.getTank(i, TankColor.Type.Red).getScaledInstance((int)(Tank.TankWidths[i-1]*Tank.TankScale[i-1]), (int)(Tank.TankHeights[i-1]*Tank.TankScale[i-1]), Image.SCALE_DEFAULT));
			tank.add(Constants.getTank(i, TankColor.Type.Green).getScaledInstance((int)(Tank.TankWidths[i-1]*Tank.TankScale[i-1]), (int)(Tank.TankHeights[i-1]*Tank.TankScale[i-1]), Image.SCALE_DEFAULT));
			tank.add(Constants.getTank(i, TankColor.Type.Blue).getScaledInstance((int)(Tank.TankWidths[i-1]*Tank.TankScale[i-1]), (int)(Tank.TankHeights[i-1]*Tank.TankScale[i-1]), Image.SCALE_DEFAULT));
			tank.add(Constants.getTank(i, TankColor.Type.Yellow).getScaledInstance((int)(Tank.TankWidths[i-1]*Tank.TankScale[i-1]), (int)(Tank.TankHeights[i-1]*Tank.TankScale[i-1]), Image.SCALE_DEFAULT));
		}
		backgrounds = new Vector<Image>();
		for (int i = 0; i < Constants.Gameboards.length; i++) {
			backgrounds.add(Constants.getBackground(i).getScaledInstance(Constants.GameBoardWidth, Constants.GameBoardHeight, Image.SCALE_DEFAULT));
		}
		panelBackground = Util.loadImage("background/panel.png").getScaledInstance(Constants.GameBoardGUIWidth-Constants.GameBoardWidth, Constants.GameBoardHeight, Image.SCALE_DEFAULT);
	}
	
	public Image getBackground(int idx) {
		return backgrounds.get(idx);
	}
	
	public Image getPanelBackground() {
		return panelBackground;
	}
	
	public Image getBullet() {
		return bullet;
	}

	
	public Image getWall() {
		return wall;
	}
	
	public Image getTank(int i, TankColor.Type type) {
		if (type == TankColor.Type.Red)
			return tank.elementAt(i*4);
		else if (type == TankColor.Type.Green) 
			return tank.elementAt(i*4+1);
		else if (type == TankColor.Type.Blue)
			return tank.elementAt(i*4+2);
		else if (type == TankColor.Type.Yellow)
			return tank.elementAt(i*4+3);
		return null;
	}
	
	public Image getWater(int i) {
		return water.elementAt(i);
	}
	
	public Image getTree(int i) {
		return tree.elementAt(i);
	}
 	
	
}
