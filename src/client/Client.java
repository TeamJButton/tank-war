package client;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;
import java.util.concurrent.locks.Condition;
// import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import gameObject.GameBoard;
import gameObject.Tank;
import gameObject.TankColor;
import networking.ClientPacket;
import networking.KeyPressedReleased;
import networking.SignIn;
import server.GameRoom;
import server.PlayerRecord;

public class Client extends JFrame {
	private static final long serialVersionUID = 2774791513657968811L;
	private LoadingGUI loadingGUI;
	private LobbyGUI lobbyGUI;
	private RoomGUI roomGUI;
	private LoginGUI loginGUI;
	private GameBoardGUI gameboardGUI;
	private GameOverGUI gameOverGUI;
	private Player player;
	static ClientListener clientlistener;
	public volatile GameBoard toRender;
	public Lock toRenderLock;
	public Condition toRenderCond;

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
		} catch (Exception e) {
			System.out.println("Warning! Cross-platform L&F not used!");
		}
		new Client();
	}

	public Client() {
		super("");
		toRender = null;
		toRenderLock = new ReentrantLock();
		toRenderCond = toRenderLock.newCondition();
		setVisible(false);
		loadingGUI = new LoadingGUI();
		loadingGUI.setVisible(true);
		lobbyGUI = new LobbyGUI(this);
		lobbyGUI.setVisible(false);
		roomGUI = new RoomGUI(this);
		roomGUI.setVisible(false);
		gameboardGUI = new GameBoardGUI(this);
		gameboardGUI.setVisible(false);
		loginGUI = new LoginGUI(this);
		loginGUI.setVisible(true);
		loadingGUI.setVisible(false);
		clientlistener = new ClientListener(this);
		gameOverGUI = new GameOverGUI();
		gameOverGUI.setVisible(false);
	}

	public void enableButton() {
		loginGUI.enableButton();
	}

	public void playerLogin(SignIn signIn) {
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.SIGNIN, signIn);
		clientlistener.sendPacket(packet);
	}

	public void guestLogin() {
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.GUEST, null);
		clientlistener.sendPacket(packet);
	}

	public void logout() {
		player = null;
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.SIGNOUT, null);
		clientlistener.sendPacket(packet);
	}

	public void backToLogin() {
		lobbyGUI.setVisible(false);
		loginGUI.setVisible(true);
	}

	public void createGameRoom(String name) {
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.CREATE_ROOM, name);
		clientlistener.sendPacket(packet);
	}

	public void joinGameRoom(int roomID) {
		Integer intID = new Integer(roomID);
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.JOIN_ROOM, intID);
		clientlistener.sendPacket(packet);
	}

	public void showLobby(Vector<GameRoom> lobby) {
		lobbyGUI.updateLobbyInfo(lobby);
		if (!lobbyGUI.isVisible()) {
			roomGUI.setVisible(false);
			loginGUI.setVisible(false);
			gameboardGUI.setVisible(false);
			lobbyGUI.setVisible(true);
		}
	}

	public void setPlayer(PlayerRecord playerRecord) {
		if (playerRecord != null) {
			player = new Player(playerRecord.name, playerRecord.score, playerRecord.isGuest);
			lobbyGUI.setPlayer(player);
			roomGUI.setPlayer(player);
		} else
			loginGUI.loginFail("Login Failed");
	}

	public Player getPlayer() {
		return player;
	}

	public void changeColor(TankColor.Type tankcolor) {
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.COLOR, tankcolor);
		clientlistener.sendPacket(packet);
	}

	public void showRoom(GameRoom room) {
		roomGUI.updateRoom(room);
		lobbyGUI.setVisible(false);
		gameboardGUI.setVisible(false);
		roomGUI.setVisible(true);
	}

	public void isReady(boolean ready) {
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.READY, null);
		if (!ready) {
			packet = new ClientPacket(ClientPacket.ResultType.NOT_READY, null);
		}
		clientlistener.sendPacket(packet);
	}

	public void goToLobby() {
		// lobbyGUI.setVisible(true);
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.QUIT_ROOM, null);
		clientlistener.sendPacket(packet);
	}

	public void kickout(int kickoutId) {
		System.out.println(kickoutId + "@@client kickout function");
		if (kickoutId != -1) {
			ClientPacket packet = new ClientPacket(ClientPacket.ResultType.KICK, new Integer(kickoutId));
			clientlistener.sendPacket(packet);
		}

	}

	public void failCreateRoom() {
		lobbyGUI.failCreateRoom();
	}

	public void failEnterRoom() {
		lobbyGUI.failEnterRoom();
	}

	public void connectionFailed() {
		int res = JOptionPane.showConfirmDialog(this, "Connection failed... Program exiting.", "Connection Failed",
				JOptionPane.DEFAULT_OPTION);

		if (res == 0) {
			System.exit(0);
		} else if (res == JOptionPane.CLOSED_OPTION) {
			System.exit(0);
		}
	}

	public void keyPressedReleased(KeyPressedReleased kpr) {
		ClientPacket packet = new ClientPacket(ClientPacket.ResultType.KEY_PRESS, kpr);
		clientlistener.sendPacket(packet);
	}

	public void showGameBoard(GameBoard board) {
		toRenderLock.lock();
		toRender = board;
		toRenderCond.signal();
		toRenderLock.unlock();
		if (!gameboardGUI.isVisible()) {
			loginGUI.setVisible(false);
			lobbyGUI.setVisible(false);
			roomGUI.setVisible(false);
			gameboardGUI.setVisible(true);
			gameboardGUI.renderBackground(board);
		}
	}

	public void end() {
		//ClientPacket packet = new ClientPacket(ClientPacket.ResultType.END, null);
		//clientlistener.sendPacket(packet);
	}

	public boolean isInGame() {
		return gameboardGUI.isVisible();
	}

	public void localUpdateGame() {
		gameboardGUI.localUpdate();
	}
	
	public void gameOver(Vector<Tank> tanks) {
		gameOverGUI.updateResults(tanks);
		gameOverGUI.setVisible(true);
	}
}

// Background Panel
class bgPane extends JPanel {
	private static final long serialVersionUID = 1L;
	private Image bgImage;

	public bgPane(int x, int y, int z) {
		setLayout(new BorderLayout());
		bgImage = util.Util.loadImage("background/metal_" + z + ".png").getScaledInstance(x, y, Image.SCALE_SMOOTH);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(bgImage, 0, 0, this);
	}
}
