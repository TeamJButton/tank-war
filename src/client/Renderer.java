package client;

import gameObject.GameBoard;

public class Renderer extends Thread {
	Client client;
	GameBoardGUI gui;
	public Renderer(Client _client, GameBoardGUI _gui) {
		client = _client;
		gui = _gui;
		start();
	}
	
	@Override
	public void run() {
		while (true) {
			client.toRenderLock.lock();
		    while (client.toRender == null) {
		      try {
		    	//client.toRenderLock.unlock();
		    	//if (client.isInGame()) {
		    	//	client.localUpdateGame();
		    	//}
		    	//client.toRenderLock.lock();
		        client.toRenderCond.await();
		      } catch (InterruptedException e) {
		        e.printStackTrace();
		      }
		    }
		    GameBoard myBoard = client.toRender;
		    client.toRender = null;
		    client.toRenderLock.unlock();
		    
			gui.refresh(myBoard);
		}
	}
}
