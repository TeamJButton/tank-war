package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.border.LineBorder;

import customerUI.BoardJPanel;
import customerUI.GameBackground;
import customerUI.GameInfoPanel;
import gameObject.GameBoard;
import gameObject.Tank;
import networking.KeyPressedReleased;
import util.Constants;

public class GameBoardGUI extends JFrame implements KeyListener {

  private static final long serialVersionUID = 1L;

  private int posX = 0, posY = 0;
  private BoardJPanel boardPanel;
  private Client mClient;
  @SuppressWarnings("unused")
private GameBoard myBoard;
  JLabel time;
  private boolean pLeft, pRight, pUp, pDown, pSpace;
  private CacheImage cImage;
  private GameBackground gamebackground;
  private GameInfoPanel infoPanel;
  private SoundPlayer soundPlayer;

  public GameBoardGUI(Client inClient) {
    super("Tank War Room");
    mClient = inClient;
    cImage = new CacheImage();
    soundPlayer = new SoundPlayer();
    setSize(Constants.GameBoardGUIWidth, Constants.GameBoardHeight);
    setLocationRelativeTo(null); // Center on the screen
    setUndecorated(true);
    setFocusable(true);
    setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // Exit Action
    setIconImages(util.Util.setIcon()); // Set Icon
	getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));


    // add GUI elements
    instantiateVariables();

    // TODO: Allow drag in window
    addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        posX = e.getX();
        posY = e.getY();
      }
    });
    addMouseMotionListener(new MouseAdapter() {
      public void mouseDragged(MouseEvent evt) {
        setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);
      }
    });
    // createGameOverGUI();
    // gameOverGUI.setVisible(true);

    pLeft = pRight = pUp = pDown = pSpace = false;
    addKeyListener(this);
    new Renderer(mClient, this);
  }

  private void instantiateVariables() {
	// BoardPanel setup
	gamebackground = new GameBackground(cImage);
	gamebackground.setSize(Constants.GameBoardWidth, Constants.GameBoardHeight);
    boardPanel = new BoardJPanel(cImage);
    boardPanel.setSize(Constants.GameBoardWidth, Constants.GameBoardHeight);
    boardPanel.setBorder(new LineBorder(null));
    infoPanel = new GameInfoPanel(cImage);
    boardPanel.add(infoPanel);
    myBoard = null;
    gamebackground.add(boardPanel, BorderLayout.CENTER);
    add(gamebackground, BorderLayout.CENTER);
  }

  public void refresh(GameBoard board) {
    myBoard = board;
    
    boardPanel.setBoard(board);
    boardPanel.revalidate();
    boardPanel.repaint();
    infoPanel.setTanks(board.getTanks());
    infoPanel.setTime(board.getTime());
    double percent = 0;
    for (Tank tank : board.getTanks()) {
    	if (tank.getMyPlayerRecord().name.equals(mClient.getPlayer().getName())) {
    		percent = tank.getReload();
    	}
    }
    percent = percent / Constants.ReloadTime;
    infoPanel.setReload(percent);
    infoPanel.revalidate();
    infoPanel.repaint();
    if (board.hitTankSound) {
    	soundPlayer.playTank();
    } else if (board.hitTreeSound) {
    	soundPlayer.playTree();
    } else if (board.shootSound) {
    	soundPlayer.playBullet();
    } else if (board.hitWallSound) {
    	soundPlayer.playWall();
    } else if (board.beepSound) {
    	soundPlayer.playBeep();
    } else if (board.startSound) {
    	soundPlayer.playStart();
    }
  }

  @Override
  public void keyPressed(KeyEvent e) {
    // System.out.println("pressed!!!");
    if (e.getKeyCode() == KeyEvent.VK_RIGHT && !pRight) {
      pRight = true;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.RIGHT, KeyPressedReleased.Type.Pressed);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_LEFT && !pLeft) {
      pLeft = true;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.LEFT, KeyPressedReleased.Type.Pressed);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_DOWN && !pDown) {
      pDown = true;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.DOWN, KeyPressedReleased.Type.Pressed);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_UP && !pUp) {
      pUp = true;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.UP, KeyPressedReleased.Type.Pressed);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_SPACE && !pSpace) {
      pSpace = true;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.SPACE, KeyPressedReleased.Type.Pressed);
      mClient.keyPressedReleased(kpr);
    }
  }

  @Override
  public void keyReleased(KeyEvent e) {
    // System.out.println("released!!!");
    if (e.getKeyCode() == KeyEvent.VK_RIGHT && pRight) {
      pRight = false;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.RIGHT, KeyPressedReleased.Type.Released);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_LEFT && pLeft) {
      pLeft = false;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.LEFT, KeyPressedReleased.Type.Released);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_DOWN && pDown) {
      pDown = false;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.DOWN, KeyPressedReleased.Type.Released);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_UP && pUp) {
      pUp = false;
      KeyPressedReleased kpr =
          new KeyPressedReleased(server.PlayerKey.UP, KeyPressedReleased.Type.Released);
      mClient.keyPressedReleased(kpr);
    } else if (e.getKeyCode() == KeyEvent.VK_SPACE && pSpace) {
      pSpace = false;
    }
  }

  @Override
  public void keyTyped(KeyEvent e) {

  }

  public void renderBackground(GameBoard board) {
	  gamebackground.update(board.getBackgroundInt(), board.getWalls(), board.getWaters(), cImage);
	  gamebackground.revalidate();
	  gamebackground.repaint();
  }
  
  public void localUpdate() {
	  boardPanel.localUpdate();
  }
}
