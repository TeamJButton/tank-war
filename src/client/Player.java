package client;

import java.awt.Image;

import gameObject.TankColor;
import util.Constants;

public class Player {
  private String name;
  private int level;
  private int exp;
  private Image tankImg;
  private boolean isGuest;

  public Player(String _name, int _score, boolean _isGuest) {
    name = _name;
    isGuest = _isGuest;
    this.level = util.Util.levelFromScore(_score);
    this.exp = _score % 1000;
    // this.tankImg = Util.loadImage("tank.png");
    this.tankImg = Constants.getTank(level, TankColor.Type.Green);
  }

  public boolean isGuest() {
    return isGuest;
  }

  public String getName() {
    return name;
  }

  public int getLevel() {
    return level;
  }

  public int getExp() {
    return exp;
  }

  public Image getImg() {
    return tankImg;
  }

  public void setExp(int _exp){
	  this.exp=_exp;
  }
  
  public void setImg(Image _tankImg) {
    this.tankImg = _tankImg;
  }

  public void setLevel(int _level) {
    this.level = _level;
  }

  public void increaseExp(int _exp) {
    this.exp += _exp;
  }
}
