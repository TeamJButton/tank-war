package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import gameObject.TankColor;
import server.GameRoom;
import util.Constants;

public class LobbyGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private int posX = 0, posY = 0;
	static int roomID = 0x0;
	private JPanel jpCenter, jpEast, jpInfo;
	private JLabel jlTitle, jlPic, jlName, jlLvl, jlExp;
	private JTable jtRoomList;
	private JScrollPane jsp;
	private JProgressBar jpbExp;
	private JButton jbEnter, jbCreate, jbLogout, jbExit;
	private DefaultTableModel dtm;
	private Client client;
	Player currPlayer;
	Vector<GameRoom> lobbyList;
	RoomCreatorGUI roomCreator;

	/**
	 * LobbyGUI Constructor
	 * 
	 * @param owner
	 *            the {@code Frame} from which the dialog is displayed
	 * @param currPlayer
	 *            the current {@code Player}
	 * @author Yifan Meng
	 */
	public LobbyGUI(Client _client) {
		super("Tank War - Game Lobby");
		client = _client;
		// TODO: Window Initialization
		setSize(660, 600);
		setContentPane(new bgPane(getWidth(), getHeight(), 2));
		setMinimumSize(new Dimension(660, 600));
		setLocationRelativeTo(null); // Center on the screen
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // Exit Action
		setIconImages(util.Util.setIcon()); // Set Icon

		// TODO: Panel Setup
		getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));
		GridBagConstraints gbc = new GridBagConstraints();
		jpCenter = new JPanel(new GridBagLayout());
		jpCenter.setOpaque(false);
		jlTitle = new JLabel("TANK WAR");
		jlTitle.setFont(new Font("Arial Black", Font.BOLD, 50));
		// TODO: JTabel Init
		Object[] jtColumn = { "Room Name", "Availability" };
		Object[][] jtData = {};
		dtm = new DefaultTableModel(jtData, jtColumn) {
			private static final long serialVersionUID = 1L;

			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		jtRoomList = new JTable(dtm);
		jtRoomList.setOpaque(false);
		jtRoomList.setRowHeight(40); // Row Height
		jtRoomList.setTableHeader(null); // No Header
		jtRoomList.setShowVerticalLines(false); // No vertical grid lines
		jtRoomList.setRowSelectionAllowed(true);
		jtRoomList.setColumnSelectionAllowed(false);
		jtRoomList.setFont(new Font("Arial", Font.PLAIN, 25)); // Font in Table
		jtRoomList.setAutoResizeMode(JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS);
		jtRoomList.getColumnModel().getColumn(1).setMaxWidth(120);
		jtRoomList.getColumnModel().getColumn(1).setPreferredWidth(120);
		jtRoomList.getColumnModel().setSelectionModel(new DefaultListSelectionModel() {
			private static final long serialVersionUID = 1L;

			@Override
			public int getLeadSelectionIndex() {
				return -1;
			}
		});
		DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 10));
				return this;
			}
		};
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
					boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
				setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 30));
				return this;
			}
		};
		rightRenderer.setHorizontalAlignment(JLabel.RIGHT); // Availability
															// align right
		jtRoomList.getColumnModel().getColumn(0).setCellRenderer(leftRenderer);
		jtRoomList.getColumnModel().getColumn(1).setCellRenderer(rightRenderer);
		jsp = new JScrollPane(jtRoomList); // Add to JScrollPane
		jsp.setOpaque(false);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jsp.setMinimumSize(new Dimension(400, 450));
		jsp.setPreferredSize(new Dimension(400, 450));
		jsp.setMaximumSize(new Dimension(400, 450));
		jpCenter.setMinimumSize(new Dimension(400, 600));
		jpCenter.setPreferredSize(new Dimension(400, 600));
		jpCenter.setMaximumSize(new Dimension(400, 600));
		jpCenter.setBorder(new EmptyBorder(0, 10, 0, 10));
		gbc.gridx = 0;
		gbc.gridy = 0;
		jpCenter.add(jlTitle, gbc);
		gbc.gridy = 1;
		jpCenter.add(jsp, gbc);
		add(jpCenter, BorderLayout.CENTER);

		jpEast = new JPanel(new GridBagLayout());
		jpEast.setOpaque(false);
		jpInfo = new JPanel(new GridBagLayout());
		jpInfo.setOpaque(false);
		jlPic = new JLabel(new ImageIcon(Constants.getTank(9, TankColor.Type.Red)));
		jlName = new JLabel("currPlayer", JLabel.CENTER);
		jlName.setFont(new Font("Arial", Font.PLAIN, 18));
		jlLvl = new JLabel("Level: ", JLabel.CENTER);
		jlLvl.setFont(new Font("Arial", Font.PLAIN, 18));
		jpbExp = new JProgressBar(0, 1000);
		jpbExp.setValue(0);
		jlExp = new JLabel("Exp: " + "/1000", JLabel.CENTER);
		jlExp.setFont(new Font("Arial", Font.PLAIN, 18));
		jbEnter = new JButton("Enter");
		jbEnter.setEnabled(false);
		jbEnter.setFont(new Font("Arial", Font.PLAIN, 18));
		jbCreate = new JButton("Create");
		jbCreate.setFont(new Font("Arial", Font.PLAIN, 18));
		jbLogout = new JButton("Sign Out");
		jbLogout.setFont(new Font("Arial", Font.PLAIN, 18));
		jbExit = new JButton("Exit Game");
		jbExit.setFont(new Font("Arial", Font.PLAIN, 18));
		jlName.setMinimumSize(new Dimension(170, 25));
		jlName.setPreferredSize(new Dimension(170, 25));
		jlName.setMaximumSize(new Dimension(170, 25));
		jpbExp.setMinimumSize(new Dimension(150, 20));
		jpbExp.setPreferredSize(new Dimension(150, 20));
		jpbExp.setMaximumSize(new Dimension(150, 20));
		jpInfo.setMinimumSize(new Dimension(180, 350));
		jpInfo.setPreferredSize(new Dimension(180, 350));
		jpInfo.setMaximumSize(new Dimension(180, 350));
		jpInfo.setBorder(BorderFactory.createLineBorder(Color.black));
		jpEast.setMinimumSize(new Dimension(200, 600));
		jpEast.setPreferredSize(new Dimension(200, 600));
		jpEast.setMaximumSize(new Dimension(200, 600));
		jpEast.setBorder(new EmptyBorder(0, 10, 0, 20));
		jbEnter.setMinimumSize(new Dimension(180, 30));
		jbEnter.setPreferredSize(new Dimension(180, 30));
		jbEnter.setMaximumSize(new Dimension(180, 30));
		jbCreate.setMinimumSize(new Dimension(180, 30));
		jbCreate.setPreferredSize(new Dimension(180, 30));
		jbCreate.setMaximumSize(new Dimension(180, 30));
		jbLogout.setMinimumSize(new Dimension(180, 30));
		jbLogout.setPreferredSize(new Dimension(180, 30));
		jbLogout.setMaximumSize(new Dimension(180, 30));
		jbExit.setMinimumSize(new Dimension(180, 30));
		jbExit.setPreferredSize(new Dimension(180, 30));
		jbExit.setMaximumSize(new Dimension(180, 30));
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 0, 15, 0);
		jpInfo.add(jlPic, gbc);
		gbc.gridy = 1;
		gbc.insets = new Insets(5, 0, 5, 0);
		jpInfo.add(jlName, gbc);
		gbc.gridy = 2;
		jpInfo.add(jlLvl, gbc);
		gbc.gridy = 3;
		jpInfo.add(jlExp, gbc);
		gbc.gridy = 4;
		jpInfo.add(jpbExp, gbc);
		gbc.gridy = 0;
		gbc.insets = new Insets(0, 0, 20, 0);
		jpEast.add(jpInfo, gbc);
		gbc.gridy = 1;
		gbc.insets = new Insets(13, 0, 0, 0);
		jpEast.add(jbEnter, gbc);
		gbc.gridy = 2;
		jpEast.add(jbCreate, gbc);
		gbc.gridy = 3;
		jpEast.add(jbLogout, gbc);
		gbc.gridy = 4;
		jpEast.add(jbExit, gbc);
		add(jpEast, BorderLayout.EAST);

		// TODO: Add Action Listener
		jbEnter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.joinGameRoom(lobbyList.elementAt(jtRoomList.getSelectedRow()).getRoomID());
			}
		});
		jbCreate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				callCreatorGUI();
			}
		});
		jbLogout.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.logout();
				client.backToLogin();
			}
		});
		jbExit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.logout();
				client.end();
				System.exit(0);
			}
		});
		jtRoomList.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (jtRoomList.getSelectedRows().length != 0)
					jbEnter.setEnabled(true);
				else
					jbEnter.setEnabled(false);
			}
		});

		// TODO: Allow drag in window
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				posX = e.getX();
				posY = e.getY();
			}
		});
		addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent evt) {
				setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);
			}
		});
	}

	void updateLobbyInfo(Vector<GameRoom> lobby) {
		this.lobbyList = lobby;
		DefaultTableModel model = (DefaultTableModel) jtRoomList.getModel();
		clearTable(model);
		for (GameRoom gr : lobbyList) {
			model.addRow(new Object[] { gr.getRoomName(), new Integer(gr.players.size()).toString() + " / 4" });
		}
		if (currPlayer != null && currPlayer.isGuest())
			jbCreate.setEnabled(false);
		else
			jbCreate.setEnabled(true);
		this.revalidate();
		this.repaint();
	}

	void setPlayer(Player _player) {
		currPlayer = _player;
		jlPic.setIcon(new ImageIcon(currPlayer.getImg().getScaledInstance(170, 170, Image.SCALE_SMOOTH)));
		jlName.setText(currPlayer.getName());
		jlLvl.setText("Level: " + currPlayer.getLevel());
		jpbExp.setValue(currPlayer.getExp());
		jlExp.setText("Exp: " + currPlayer.getExp() + "/1000");
		if (currPlayer != null && currPlayer.isGuest())
			jbCreate.setEnabled(false);
		else
			jbCreate.setEnabled(true);
		this.revalidate();
		this.repaint();
	}

	void failEnterRoom() {
		JOptionPane.showMessageDialog(this, "Join Room Failed", "ERROR", JOptionPane.ERROR_MESSAGE);
	}

	void failCreateRoom() {
		roomCreator.CreateFail();
	}

	// TODO: Call Creator GUI
	private void callCreatorGUI() {
		roomCreator = new RoomCreatorGUI(this, client);
	}

	void clearTable(DefaultTableModel model) {
		if (model.getRowCount() > 0) {
			for (int i = model.getRowCount() - 1; i > -1; i--) {
				model.removeRow(i);
			}
		}
	}
}
