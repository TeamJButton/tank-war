package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import networking.SignIn;
import util.Constants;

public class LoginGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private GridBagConstraints gbc;
	private JLabel /*jlTitle,*/ jlPic, jlUser;
	private static JLabel jlFail;
	private JTextField jtfUser;
	private JLabel jlPass;
	private JPasswordField jpfPass;
	private JButton jbLogin, jbGuest, jbReg;
	private Client client;

	public LoginGUI(Client _client) {
		// TODO: Window Initialization
		super("Tank War"); // Window Title
		client = _client;
		setSize(600, 500);
		setMinimumSize(new Dimension(600, 500));
		setResizable(false);
		setLocationRelativeTo(null); // Center on the screen
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Exit Action
		setIconImages(util.Util.setIcon()); // Set Icon

		// TODO: Background Panel
		setContentPane(new bgPane(getWidth(), getHeight(), 2));

		// TODO: Panel Setup
		setLayout(new GridBagLayout());
		gbc = new GridBagConstraints();
		/*jlTitle = new JLabel("TANK WAR", JLabel.CENTER);
		jlTitle.setFont(new Font("Arial Black", Font.BOLD, 50));*/
		jlPic = new JLabel(
				new ImageIcon(util.Util.loadImage("login.png").getScaledInstance(380, 230, Image.SCALE_SMOOTH)));
		jlFail = new JLabel("Connecting to " + Constants.host + " ...");
		jlFail.setForeground(Color.red);
		jlFail.setFont(new Font("Arial", Font.BOLD, 18));
		jlUser = new JLabel("Username: ");
		jlUser.setFont(new Font("Arial", Font.BOLD, 18));
		jtfUser = new JTextField();
		jlPass = new JLabel("Password: ");
		jlPass.setFont(new Font("Arial", Font.BOLD, 18));
		jpfPass = new JPasswordField();
		jbLogin = new JButton("Login");
		jbLogin.setEnabled(false);
		jbGuest = new JButton("Guest");
		jbGuest.setEnabled(false);
		jbReg = new JButton("Register");
		jbReg.setEnabled(false);
		jtfUser.setMinimumSize(new Dimension(250, 28));
		jtfUser.setPreferredSize(new Dimension(250, 28));
		jtfUser.setMaximumSize(new Dimension(250, 28));
		jpfPass.setMinimumSize(new Dimension(250, 28));
		jpfPass.setPreferredSize(new Dimension(250, 28));
		jpfPass.setMaximumSize(new Dimension(250, 28));
		jbLogin.setMinimumSize(new Dimension(200, 28));
		jbLogin.setPreferredSize(new Dimension(200, 28));
		jbLogin.setMaximumSize(new Dimension(200, 28));
		jbGuest.setMinimumSize(new Dimension(200, 28));
		jbGuest.setPreferredSize(new Dimension(200, 28));
		jbGuest.setMaximumSize(new Dimension(200, 28));
		jbReg.setMinimumSize(new Dimension(200, 28));
		jbReg.setPreferredSize(new Dimension(200, 28));
		jbReg.setMaximumSize(new Dimension(200, 28));
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.gridwidth = 2;
		//add(jlTitle, gbc);
		gbc.gridy = 1;
		gbc.insets = new Insets(0, 0, 5, 0);
		add(jlPic, gbc);
		gbc.gridy = 2;
		add(jlFail, gbc);
		gbc.gridy = 3;
		gbc.gridwidth = 1;
		gbc.insets = new Insets(0, 0, 10, 0);
		add(jlUser, gbc);
		gbc.gridx = 1;
		add(jtfUser, gbc);
		gbc.gridx = 0;
		gbc.gridy = 4;
		add(jlPass, gbc);
		gbc.gridx = 1;
		gbc.insets = new Insets(0, 0, 20, 0);
		add(jpfPass, gbc);
		gbc.gridx = 0;
		gbc.gridy = 5;
		gbc.gridwidth = 2;
		gbc.insets = new Insets(0, 0, 10, 0);
		add(jbLogin, gbc);
		gbc.gridy = 6;
		add(jbGuest, gbc);
		gbc.gridy = 7;
		add(jbReg, gbc);

		// TODO: Add Action Listeners
		jbLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Add Login Action
				System.out.println("trying to login");
				jlFail.setText(" ");
				if (jtfUser.getText() != null && jpfPass.getPassword() != null) {
					SignIn login = new SignIn(jtfUser.getText(), jpfPass.getPassword(), false);
					client.playerLogin(login);
					jpfPass.setText("");
				}
			}
		});
		jbGuest.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.guestLogin();
			}
		});
		jbReg.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showRegGUI();
			}
		});
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				client.end();
				System.exit(0);
			}
		});
	}

	static void loginFail(String str) {
		jlFail.setText(str);
	}

	void enableButton() {
		jlFail.setText(" ");
		jbLogin.setEnabled(true);
		jbGuest.setEnabled(true);
		jbReg.setEnabled(true);
	}

	// TODO: Call Register GUI
	private void showRegGUI() {
		new RegisterGUI(this, client);
	}

}