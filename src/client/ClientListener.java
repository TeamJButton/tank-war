package client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Vector;

import gameObject.Tank;
import networking.ClientPacket;
import networking.CompressedBoard;
import networking.ServerPacket;
import server.GameRoom;
import server.PlayerRecord;
import util.Constants;

public class ClientListener extends Thread {
	private Socket socket;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private Client client;
	private boolean connected;

	public ClientListener(Client _client) {
		client = _client;
		connected = false;
		try {
			socket = new Socket(Constants.host, Constants.port);
			initializeVariables();
			connected = true;
			client.enableButton();
		} catch (UnknownHostException e) {
			tryAgain();
			// client.connectionFailed();
			// e.printStackTrace();
		} catch (IOException e) {
			tryAgain();
			// client.connectionFailed();
			// e.printStackTrace();
		}
		start();
	}

	private void tryAgain() {
		try {
			socket = null;
			LoginGUI.loginFail("Connecting to " + Constants.host2 + " ...");
			socket = new Socket(Constants.host2, Constants.port);
			System.out.println("Again");
			initializeVariables();
			connected = true;
			client.enableButton();
		} catch (UnknownHostException ee) {
			client.connectionFailed();
			ee.printStackTrace();
		} catch (IOException ee) {
			client.connectionFailed();
			ee.printStackTrace();
		}
	}

	private void initializeVariables() throws IOException {
		oos = new ObjectOutputStream(socket.getOutputStream());
		oos.flush();
		ois = new ObjectInputStream(socket.getInputStream());
	}

	public void sendPacket(ClientPacket packet) {
		if (packet.type == ClientPacket.ResultType.KEY_PRESS)
			System.out.print("");
		try {
			oos.writeObject(packet);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void run() {
		try {
			ServerPacket packet;
			while (connected) {
				String s = (String) ois.readObject();
				if (s == null)
					continue;
				Object received = (Object) util.Util.serializeFromString(s);
				if (received instanceof char[]) {
					CompressedBoard cb = new CompressedBoard((char[]) received);
					client.showGameBoard(cb.board);
					continue;
				}
				packet = (ServerPacket) received;
				if (packet == null)
					continue;
				if (packet.type == ServerPacket.ResultType.SIGN) {
					// System.out.println("Sign");
					PlayerRecord pr = (PlayerRecord) packet.object;
					client.setPlayer(pr);
				} else if (packet.type == ServerPacket.ResultType.LOBBY) {
					// System.out.println("Lobby");
					Vector<GameRoom> room = (Vector<GameRoom>) packet.object;
					client.showLobby(room);
				} else if (packet.type == ServerPacket.ResultType.ROOM) {
					// System.out.println("Room");
					if (packet.success) {
						GameRoom room = (GameRoom) packet.object;
						// GameRoom room = (GameRoom)
						// util.Util.serializeFromString((String)packet.object);
						// System.out.println(room.players.get(0).getPlayerRecord().color.toString()+"@@@packet.success");
						client.showRoom(room);
					} else if (packet.message.equals("Failed to join room")) {
						client.failEnterRoom();
					} else if (packet.message.equals("Failed to create room")) {
						client.failCreateRoom();
					}

				} else if (packet.type == ServerPacket.ResultType.GAME) {
					CompressedBoard cb = (CompressedBoard) packet.object;
					cb.decompressBoard();
					client.showGameBoard(cb.board);
				} else if (packet.type == ServerPacket.ResultType.OVER) {
					System.out.println("received game over results");
					Vector<Tank> tanks = (Vector<Tank>) packet.object;
					client.gameOver(tanks);
				}
			}
		} catch (IOException ioe) {
			connected = false;
			ioe.printStackTrace();
			client.connectionFailed();
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
	}
}
