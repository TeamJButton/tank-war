package client;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import util.Constants;

public class SoundPlayer {
	
	private void playSound(String file) {
		try {
		    AudioInputStream audioInputStream =
		        AudioSystem.getAudioInputStream(
		            this.getClass().getResource("/sound/"+file));
		    Clip clip = AudioSystem.getClip();
		    clip.open(audioInputStream);
		    clip.start();
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void playBeep() {
		playSound(Constants.BeepSound);
	}
	
	public void playStart() {
		playSound(Constants.StartSound);
	}
	
	public void playTank() {
		playSound(Constants.TankSound);
	}
	
	public void playTree() {
		playSound(Constants.TreeSound);
	}
	
	public void playWall() {
		playSound(Constants.WallSound);
	}
	
	public void playBullet() {
		playSound(Constants.BulletSound);
	}
}
