package client;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class LoadingGUI extends JFrame {
	private static final long serialVersionUID = 1L;

	public LoadingGUI() {
		super("Tank War Loading");
		setSize(500, 500);
		setMinimumSize(new Dimension(500, 500));
		setMaximumSize(new Dimension(500, 500));
		setResizable(false);
		setUndecorated(true);
		setAlwaysOnTop(true);
		setLocationRelativeTo(null); // Center on the screen
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Exit Action
		setIconImages(util.Util.setIcon()); // Set Icon
		add(new JLabel(new ImageIcon(util.Util.loadImage("loading_n8.png"))));
		this.setVisible(true);
	}
}
