package client;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import networking.SignIn;

public class RegisterGUI extends JDialog {
  private static final long serialVersionUID = 1L;
  JDialog self = this;
  private GridBagConstraints gbc;
  private JLabel jlUser;
  private Container jlTitle;
  private JLabel jlFail;
  private JPasswordField jpfPass;
  private JLabel jlPass;
  private JTextField jtfUser;
  private JLabel jlConfirm;
  private JPasswordField jpfConfirm;
  private JButton jbCreate;
  private Client client;

  /**
   * RegisterGUI Constructor
   * 
   * @param owner the {@code Frame} from which the dialog is displayed
   * @author Yifan Meng
   */
  public RegisterGUI(Frame owner, Client _client) {
    super(owner, "Register", true);
    client = _client;
    // TODO: Window Initialization
    setSize(400, 400);
    setMinimumSize(new Dimension(400, 400));
    setLocationRelativeTo(null); // Center on the screen
    setDefaultCloseOperation(DISPOSE_ON_CLOSE); // Exit Action


    // TODO: Panel Setup
    setLayout(new GridBagLayout());
    gbc = new GridBagConstraints();
    jlTitle = new JLabel("Register New User", JLabel.CENTER);
    jlTitle.setFont(new Font("Arial", Font.BOLD, 30));
    jlFail = new JLabel(" ", JLabel.CENTER);
    jlFail.setForeground(Color.red);
    jlFail.setFont(new Font("Arial", Font.PLAIN, 16));
    jlUser = new JLabel("Username: ", JLabel.CENTER);
    jlUser.setFont(new Font("Arial", Font.PLAIN, 18));
    jtfUser = new JTextField();
    jlPass = new JLabel("Password: ", JLabel.CENTER);
    jlPass.setFont(new Font("Arial", Font.PLAIN, 18));
    jpfPass = new JPasswordField();
    jlConfirm = new JLabel("Confirm Pw: ", JLabel.CENTER);
    jlConfirm.setFont(new Font("Arial", Font.PLAIN, 18));
    jpfConfirm = new JPasswordField();
    jbCreate = new JButton("Create New User");
    jlFail.setMinimumSize(new Dimension(380, 28));
    jlFail.setPreferredSize(new Dimension(380, 28));
    jlFail.setMaximumSize(new Dimension(380, 28));
    jlUser.setMinimumSize(new Dimension(160, 28));
    jlUser.setPreferredSize(new Dimension(160, 28));
    jlUser.setMaximumSize(new Dimension(160, 28));
    jlPass.setMinimumSize(new Dimension(160, 28));
    jlPass.setPreferredSize(new Dimension(160, 28));
    jlPass.setMaximumSize(new Dimension(160, 28));
    jlConfirm.setMinimumSize(new Dimension(160, 28));
    jlConfirm.setPreferredSize(new Dimension(160, 28));
    jlConfirm.setMaximumSize(new Dimension(160, 28));
    jtfUser.setMinimumSize(new Dimension(200, 28));
    jtfUser.setPreferredSize(new Dimension(200, 28));
    jtfUser.setMaximumSize(new Dimension(200, 28));
    jpfPass.setMinimumSize(new Dimension(200, 28));
    jpfPass.setPreferredSize(new Dimension(200, 28));
    jpfPass.setMaximumSize(new Dimension(200, 28));
    jpfConfirm.setMinimumSize(new Dimension(200, 28));
    jpfConfirm.setPreferredSize(new Dimension(200, 28));
    jpfConfirm.setMaximumSize(new Dimension(200, 28));
    jbCreate.setMinimumSize(new Dimension(200, 28));
    jbCreate.setPreferredSize(new Dimension(200, 28));
    jbCreate.setMaximumSize(new Dimension(200, 28));
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 2;
    gbc.insets = new Insets(0, 0, 20, 0);
    add(jlTitle, gbc);
    gbc.gridy = 1;
    gbc.insets = new Insets(0, 0, 15, 0);
    add(jlFail, gbc);
    gbc.gridy = 2;
    gbc.gridwidth = 1;
    add(jlUser, gbc);
    gbc.gridx = 1;
    add(jtfUser, gbc);
    gbc.gridx = 0;
    gbc.gridy = 3;
    add(jlPass, gbc);
    gbc.gridx = 1;
    add(jpfPass, gbc);
    gbc.gridx = 0;
    gbc.gridy = 4;
    gbc.insets = new Insets(0, 0, 40, 0);
    add(jlConfirm, gbc);
    gbc.gridx = 1;
    add(jpfConfirm, gbc);
    gbc.gridx = 0;
    gbc.gridy = 5;
    gbc.gridwidth = 2;
    gbc.insets = new Insets(0, 0, 0, 0);
    add(jbCreate, gbc);

    // TODO: Add Action Listener
    jbCreate.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        jlFail.setText("");
        if (jtfUser.getText().length() == 0) {
          jlFail.setText("Username cannot be empty");
        } else if (!Arrays.equals(jpfPass.getPassword(), jpfConfirm.getPassword())) {
          jlFail.setText("Passwords do not match");
        } else if (!networking.SignIn.validatePassword(jpfPass.getPassword())) {
          jlFail.setText("Password requires: 1 number, 1 UPPERCASE letter");
        } else {
          SignIn signup = new SignIn(jtfUser.getText(), jpfPass.getPassword(), true);
          client.playerLogin(signup);
          self.dispose();
        }
      }
    });
    setVisible(true);
  }
}
