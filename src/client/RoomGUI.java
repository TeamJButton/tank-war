package client;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.TitledBorder;

import gameObject.TankColor;
import server.GameRoom;
import server.PlayerRecord;
import server.ServerClientCommunicator;
import util.Constants;
import util.Util;

public class RoomGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private int posX = 0, posY = 0;
	private int mode;
	private Player player;
	private JLabel usernameLbl;
	private JLabel levelLbl;
	private JProgressBar scoreBar;
	private CustomBtn color1Btn;
	private CustomBtn color2Btn;
	private CustomBtn color3Btn;
	private CustomBtn color4Btn;
	private CustomBtn player1Btn;
	private CustomBtn player2Btn;
	private CustomBtn player3Btn;
	private CustomBtn player4Btn;
	private JButton exitBtn;
	private JButton yBtn;
	private JButton nBtn;
	private CustomBtn selectPlayerBtn;
	private JPanel roomPanel;
	private JPanel leftPanel;
	private JPanel rightPanel;
	private JPanel topPanel;
	private JPanel topLeftPanel;
	private JPanel rightMiddlePanel;
	private JPanel rightBottomPanel;
	private TitledBorder ExpTitle;
	private Client client;

	public RoomGUI(Client _client) {
		super("Tank War");
		client = _client;
		setSize(1000, 700);
		setContentPane(new bgPane(getWidth(), getHeight(), 3));
		setFocusable(true);
		setLocationRelativeTo(null); // Center on the screen
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // can't close
		setIconImages(util.Util.setIcon()); // Set Icon
		getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, 3));

		initializeVariables();
		createGUI();
		addActionListeners();
	}

	private void initializeVariables() {
		player = null;
		mode = 2;
		usernameLbl = new JLabel("Name: ");
		usernameLbl.setFont(Constants.ArialPlain);
		levelLbl = new JLabel("Level: 1");
		levelLbl.setFont(Constants.ArialPlain);
		scoreBar = new JProgressBar(0, 1000);
		scoreBar.setOpaque(false);
		scoreBar.setValue(0);
		scoreBar.setStringPainted(true);
		ExpTitle = new TitledBorder("Exp: 0/1000");
		ExpTitle.setTitleFont(Constants.ArialItalic);
		scoreBar.setBorder(ExpTitle);

		color1Btn = new CustomBtn("Red", Constants.getTank(1, TankColor.Type.Red), 35, 20);
		color2Btn = new CustomBtn("Green", Constants.getTank(1, TankColor.Type.Green), 35, 20);
		color3Btn = new CustomBtn("Blue", Constants.getTank(1, TankColor.Type.Blue), 35, 20);
		color4Btn = new CustomBtn("Yellow", Constants.getTank(1, TankColor.Type.Yellow), 35, 20);

		player1Btn = new CustomBtn("", null, 70, 60);
		player1Btn.setVisible(false);
		player2Btn = new CustomBtn("", null, 70, 60);
		player2Btn.setVisible(false);
		player3Btn = new CustomBtn("", null, 70, 60);
		player3Btn.setVisible(false);
		player4Btn = new CustomBtn("", null, 70, 60);
		player4Btn.setVisible(false);

		selectPlayerBtn = new CustomBtn("", null);

		exitBtn = new JButton("Exit Room");
		exitBtn.setFont(Constants.ArialPlain);
		yBtn = new JButton("Ready!");
		yBtn.setFont(Constants.ArialPlain);
		nBtn = new JButton("Cancel!");
		nBtn.setFont(Constants.ArialPlain);

		nBtn.setEnabled(false);
	}

	private void createGUI() {
		roomPanel = new JPanel();
		roomPanel.setOpaque(false);
		add(roomPanel);

		roomPanel.setLayout(new GridLayout());

		leftPanel = new JPanel();
		leftPanel.setOpaque(false);
		TitledBorder title = new TitledBorder("name");
		title.setTitleFont(Constants.ArialItalic);
		leftPanel.setBorder(title);
		leftPanel.setLayout(new GridLayout(2, 2));
		leftPanel.add(player1Btn);
		leftPanel.add(player2Btn);
		leftPanel.add(player3Btn);
		leftPanel.add(player4Btn);
		roomPanel.add(leftPanel);

		rightPanel = new JPanel();
		rightPanel.setOpaque(false);
		rightPanel.setLayout(new GridLayout(0, 1));
		TitledBorder title2 = new TitledBorder("user info");
		title2.setTitleFont(Constants.ArialItalic);
		rightPanel.setBorder(title2);

		topPanel = new JPanel();
		topPanel.setOpaque(false);
		topPanel.setLayout(new GridLayout(1, 2));

		topLeftPanel = new JPanel();
		topLeftPanel.setOpaque(false);
		topLeftPanel.setLayout(new GridLayout(2, 1));
		topLeftPanel.add(usernameLbl);
		topLeftPanel.add(levelLbl);

		topPanel.add(topLeftPanel);
		topPanel.add(exitBtn);

		rightPanel.add(topPanel);
		rightPanel.add(scoreBar);

		rightMiddlePanel = new JPanel();
		rightMiddlePanel.setOpaque(false);
		rightMiddlePanel.setLayout(new GridLayout(0, 4));
		TitledBorder title3 = new TitledBorder("Choose your tank color:");
		title3.setTitleFont(Constants.ArialItalic);
		rightMiddlePanel.setBorder(title3);
		rightMiddlePanel.add(color1Btn);
		rightMiddlePanel.add(color2Btn);
		rightMiddlePanel.add(color3Btn);
		rightMiddlePanel.add(color4Btn);

		rightPanel.add(rightMiddlePanel);

		rightBottomPanel = new JPanel();
		rightBottomPanel.setOpaque(false);
		rightBottomPanel.setLayout(new GridLayout(1, 2));
		rightBottomPanel.add(nBtn);
		rightBottomPanel.add(yBtn);

		rightPanel.add(rightBottomPanel);

		roomPanel.add(rightPanel);
	}

	private void addActionListeners() {
		player1Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (player1Btn.getText() != player.getName() && mode == 1) {
					selectPlayerBtn = player1Btn;
					nBtn.setEnabled(true);
				} else {
					nBtn.setEnabled(false);
					selectPlayerBtn = new CustomBtn("", null);
				}
			}
		});
		player2Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (player2Btn.getText() != player.getName() && mode == 1) {
					selectPlayerBtn = player2Btn;
					nBtn.setEnabled(true);
				}
			}
		});
		player3Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (player3Btn.getText() != player.getName() && mode == 1) {
					selectPlayerBtn = player3Btn;
					nBtn.setEnabled(true);
				}
			}
		});
		player4Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (player4Btn.getText() != player.getName() && mode == 1) {
					selectPlayerBtn = player4Btn;
					nBtn.setEnabled(true);
				}
			}
		});
		yBtn.addActionListener(new ActionListener() {// start or Ready
			public void actionPerformed(ActionEvent ae) {
				client.isReady(true);
			}
		});

		nBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				if (mode == 1) {// kick out
					System.out.println("tryign to kick out" + selectPlayerBtn.getName());
					client.kickout(selectPlayerBtn.getID());
					selectPlayerBtn = new CustomBtn("", null);
				} else {// cancel Ready
					client.isReady(false);
				}
			}
		});

		exitBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				client.goToLobby();
			}
		});

		color1Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				client.changeColor(TankColor.Type.Red);
			}
		});

		color2Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				client.changeColor(TankColor.Type.Green);
			}
		});

		color3Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				client.changeColor(TankColor.Type.Blue);
			}
		});

		color4Btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				client.changeColor(TankColor.Type.Yellow);
			}
		});

		// Allow drag in window
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				posX = e.getX();
				posY = e.getY();
			}
		});
		addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent evt) {
				setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);
			}
		});
	}

	public void setPlayer(Player _player) {
		player = _player;
		usernameLbl.setText("Name: " + player.getName());
		levelLbl.setText("Level: " + player.getLevel());
		scoreBar.setValue(player.getExp());
		ExpTitle.setTitle("Exp: " + player.getExp() + "/1000");
		// scoreBar.setBorder(title4);
		color1Btn.update("Red", Constants.getTank(player.getLevel(), TankColor.Type.Red));
		color2Btn.update("Green", Constants.getTank(player.getLevel(), TankColor.Type.Green));
		color3Btn.update("Blue", Constants.getTank(player.getLevel(), TankColor.Type.Blue));
		color4Btn.update("Yellow", Constants.getTank(player.getLevel(), TankColor.Type.Yellow));
	}

	public void updateRoom(GameRoom room) {
		if (room.getRoomName() != null) {
			TitledBorder title = new TitledBorder("Room Name: " + room.getRoomName());
			title.setTitleFont(Constants.ArialItalic);
			leftPanel.setBorder(title);
		}
		int count = 1;
		boolean allReady = true;
		for (ServerClientCommunicator sc : room.players) {
			int level = Util.levelFromScore(sc.getPlayerRecord().score);
			Image img = Constants.getTank(level, TankColor.Type.Green);
			if (sc.getPlayerRecord().color == TankColor.Type.Red) {
				img = Constants.getTank(level, TankColor.Type.Red);
			} else if (sc.getPlayerRecord().color == TankColor.Type.Blue) {
				img = Constants.getTank(level, TankColor.Type.Blue);
			} else if (sc.getPlayerRecord().color == TankColor.Type.Yellow) {
				img = Constants.getTank(level, TankColor.Type.Yellow);
			}
			if (count == 1) {
				player1Btn.update(sc.getPlayerRecord().name, img);
				player1Btn.setID(sc.getPlayerRecord().id);
			} else if (count == 2) {
				player2Btn.update(sc.getPlayerRecord().name, img);
				player2Btn.setID(sc.getPlayerRecord().id);
			} else if (count == 3) {
				player3Btn.update(sc.getPlayerRecord().name, img);
				player3Btn.setID(sc.getPlayerRecord().id);
			} else if (count == 4) {
				player4Btn.update(sc.getPlayerRecord().name, img);
				player4Btn.setID(sc.getPlayerRecord().id);
			}
			if (sc.getPlayerRecord().name.equals(player.getName())) {
				if (sc.playerIsCreator()) {
					mode = 1;
					yBtn.setText("Start!");
					nBtn.setText("Kick Out!");
				} else {
					mode = 2;
					yBtn.setText("Ready!");
					nBtn.setText("Cancel!");
					if (sc.playerIsReady()) {
						yBtn.setEnabled(false);
						nBtn.setEnabled(true);
					} else {
						yBtn.setEnabled(true);
						nBtn.setEnabled(false);
					}
				}
				if (player.getLevel() != level) {
					color1Btn.update("Red", Constants.getTank(level, TankColor.Type.Red));
					color2Btn.update("Green", Constants.getTank(level, TankColor.Type.Green));
					color3Btn.update("Blue", Constants.getTank(level, TankColor.Type.Blue));
					color4Btn.update("Yellow", Constants.getTank(level, TankColor.Type.Yellow));
					player.setLevel(level);
					levelLbl.setText("Level: " + level);
				}
				player.setExp(sc.getPlayerRecord().score % 1000);
				scoreBar.setValue(player.getExp());
				ExpTitle.setTitle("Exp: " + player.getExp() + "/1000");
				PlayerRecord updatePlayerinfo = new PlayerRecord(player.getName(), sc.getPlayerRecord().score);
				updatePlayerinfo.isGuest = player.isGuest();
				client.setPlayer(updatePlayerinfo);
			} else {
				if (!sc.playerIsReady()) {
					allReady = false;
				}
			}
			count++;
		}
		if (count <= 4) {
			player4Btn.delete();
		}
		if (count <= 3) {
			player3Btn.delete();
		}
		if (count <= 2) {
			player2Btn.delete();
		}
		if (count == 1) {
			player1Btn.delete();
		}
		if (mode == 1) {
			if (allReady && room.players.size() != 1) {
				yBtn.setEnabled(true);
			} else {
				yBtn.setEnabled(false);
			}
			if (selectPlayerBtn.getID() == -1) {// can't kick out until chose
												// which player
				nBtn.setEnabled(false);
			}
		}
	}
}
