package client;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JButton;

class CustomBtn extends JButton {
  private static final long serialVersionUID = 1L;
  String str;
  Image img;
  Font font = new Font("Arial", Font.PLAIN, 20);
  private JButton self=this;
  private int id;
  private int xpad, ypad;

  public CustomBtn(String str, Image img) {
    this.str = str;
    this.img = img;
    this.id=-1;
    xpad = ypad = 15;
  }
  
  public CustomBtn(String str, Image img, int _xpad, int _ypad) {
	  this.str = str;
	  this.img = img;
	  this.id=-1;
	  xpad = _xpad;
	  ypad = _ypad;
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    this.setOpaque(false);
    this.setContentAreaFilled(false);
    setText(str);
    if (img != null) {
      g.drawImage(img, xpad, ypad, this.getWidth()-2*xpad, this.getHeight()-2*ypad, null);
    }
    g.setFont(font);
    FontMetrics fm = getFontMetrics(font);
    g.drawString(str, getWidth() / 2 - fm.stringWidth(str) / 2, getHeight() - 5);
    revalidate();
    repaint();
  }

  public void delete() {
    img = null;
    this.str = "";
    revalidate();
    repaint();
    self.setVisible(false);
    id=-1;
  }
  
  public void setText(String _str){
	  this.str=_str;
	  revalidate();
	  validate();
  }
  
  public String getText(){
	  return str;
  }
  
  public void update(String _str, Image _img){
	  img = _img;
	  this.str =_str;
	  revalidate();
	  repaint();
	  self.setVisible(true);
  }
  public void setID(int _id){
	  id=_id;
  }
  public int getID(){
	  return id;
  }
}
