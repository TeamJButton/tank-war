package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import networking.ServerPacket;
import util.Constants;

public class ServerListener extends Thread {
	
	private TankServer server;
	private ServerSocket serverSocket;
	private Vector<ServerClientCommunicator> sccVector;
	private Lock sccLock;
	private boolean thread_alive;
	
	public ServerListener(TankServer _server) throws IOException {
		server = _server;
		serverSocket = new ServerSocket(Constants.port);
		sccVector = new Vector<ServerClientCommunicator>();
		thread_alive = true;
		sccLock = new ReentrantLock();
		start();
	}
	
	@Override
	public void run() {
		while (thread_alive) {
			try {
				Socket s = serverSocket.accept();
				sccVector.add(new ServerClientCommunicator(s, this, server));
			} catch (IOException e) {
				thread_alive = false;
				e.printStackTrace();
			}
		}
	}
	
	public boolean userLoggedIn(String name) {
		for (int i = 0; i < sccVector.size(); i++) {
			if (sccVector.get(i).getPlayerName().equals(name))
				return true;
		}
		return false;
	}
	
	public void removeServerClientCommunicator(ServerClientCommunicator scc) {
		sccLock.lock();
		sccVector.remove(scc);
		System.gc();
		sccLock.unlock();
	}
	
	public void lobbyNotifyAll(Vector<GameRoom> lobby) {
		ServerPacket packet = new ServerPacket(ServerPacket.ResultType.LOBBY, lobby, "Lobby updated");
		String s = util.Util.serializeToString(packet);
		for (ServerClientCommunicator scc : sccVector) {
			if (scc.thread_alive() && scc.getPlayerState() == ServerClientCommunicator.State.AT_LOBBY) {
				scc.sendSerializedObject(s);
			}
		}
		System.gc();
	}
}
