package server;

import java.io.Serializable;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gameObject.GameBoard;
import networking.CompressedBoard;
import networking.ServerPacket;

public class GameRoom implements Serializable {
  private static final long serialVersionUID = 6052089422364622044L;
  public Vector<ServerClientCommunicator> players;
  transient private Lock lock;
  transient private TankServer server;
  private int id;
  private String roomName;
  private static int shared_id = 0;
  @SuppressWarnings("unused")
private transient GameEngine engine;

  public GameRoom(TankServer _server, String _roomName) {
    server = _server;
    roomName = _roomName;
    players = new Vector<ServerClientCommunicator>();
    lock = new ReentrantLock();
    id = shared_id;
    shared_id++;
  }

  public boolean addPlayer(ServerClientCommunicator ssc) {
    lock.lock();
    boolean success;
    if (players.size() < 4) {
      players.add(ssc);
      success = true;
    } else {
      success = false;
    }
    lock.unlock();
    if (success)
      roomNotifyAll();
    return success;
  }

  public void removePlayer(ServerClientCommunicator scc) {
    lock.lock();
    players.remove(scc);
    scc.setPlayerState(ServerClientCommunicator.State.AT_LOBBY);
    if (players.isEmpty()) {
      server.removeRoom(this);
      server.addMessage("Remove room " + getRoomName());
    } else {
      if (scc.playerIsCreator()) {
        players.get(0).setIsCreator();
      }
      roomNotifyAll();
    }
    lock.unlock();
    server.lobbyNotifyAll();
  }

  public void kick(int kickedID) {
    lock.lock();
    ServerClientCommunicator scc = null;
    for (int i = 0; i < players.size(); i++) {
      if (players.get(i).getPlayerRecord().id == kickedID) {
        scc = players.get(i);
      }
    }
    lock.unlock();
    if (scc != null) {
      scc.quitRoom();
      removePlayer(scc);
    }
  }

  public int getRoomID() {
    return id;
  }

  public String getRoomName() {
    return roomName;
  }

  public void roomNotifyAll() {
    ServerPacket result = new ServerPacket(ServerPacket.ResultType.ROOM, this, "Game room updated");
    lock.lock();
    for (int i = 0; i < players.size(); i++) {
      players.get(i).sendObject(result);
    }
    lock.unlock();
    server.addMessage("Game room " + roomName + " has " + players.size() + " players");
  }

  public void checkStart() {
    boolean start = true;
    if (players.size() >= 2) {
      for (int i = 0; i < players.size(); i++)
        if (!players.get(i).playerIsReady())
          start = false;
    } else {
      start = false;
    }
    if (start) {
      for (ServerClientCommunicator scc : players) {
//        System.out.print(scc.getPlayerName() + " SCC in GAME  :");
        scc.setPlayerState(ServerClientCommunicator.State.IN_GAME);
//        System.out.println(scc.getPlayerState().toString());
      }
      roomNotifyAll();
      server.removeRoom(this);
      startGame();
    } else
      roomNotifyAll();
  }

  public void startGame() {
    for (ServerClientCommunicator scc : players) {
//      System.out.println(scc.getName() + " " + scc.getPlayerState().toString());
      scc.setIsReady(false);
    }
    engine = new GameEngine(this);
  }

  public void notifyPlayers(GameBoard board) {
	  CompressedBoard cb = new CompressedBoard(board);
	  //ServerPacket packet = new ServerPacket(ServerPacket.ResultType.GAME, cb, "Update Game");
	  String s = util.Util.serializeToString(cb.compressed);
	  //System.out.println("board length "+s.length());
	  for (ServerClientCommunicator scc : players) {
		  scc.sendBoard(s);
	  }
  }

  public void gameOver() {
    engine = null;
	System.gc();
    server.addRoom(this);
    for (ServerClientCommunicator scc : players) {
//      System.out.println(scc.getPlayerName() + "SCC in ROOM");
      scc.setPlayerState(ServerClientCommunicator.State.IN_ROOM);
    }
    roomNotifyAll();
  }
}
