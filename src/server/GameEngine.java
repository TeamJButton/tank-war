package server;

import java.awt.geom.Point2D;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gameObject.Bullet;
import gameObject.GameBoard;
import gameObject.ObjectShape;
import gameObject.Tank;
import gameObject.Tree;
import gameObject.Wall;
import gameObject.Water;
import networking.ServerPacket;
import util.Constants;
import util.Util;
import util.Util.Transformation;

// TODO add quit people logic
public class GameEngine extends Thread {
	private volatile GameRoom room;
	private volatile GameBoard board;
	public Lock lock;
	private volatile long startTime;
	private volatile long timeRemaining;
	private long lastUpdate;

	public class TankCompare implements Comparator<Tank> {
		public int compare(Tank o1, Tank o2) {
			if (o1.getKillDeath() == o2.getKillDeath()) {
				return o2.getDeaths() - o1.getDeaths();
			} else {
				return o1.getKillDeath() - o2.getKillDeath();
			}
		}
	};

	public enum State {
		Pending, Three, Two, One, Zero, Playing, Finished
	};

	public volatile State state;

	public GameEngine(GameRoom room) {
		this.room = room;
		timeRemaining = Constants.GameDuration;
		state = State.Pending;
		board = new GameBoard(room);
		lock = new ReentrantLock();
		start();
	}

	public long getTimeRemains() {
		return timeRemaining;
	}

	public GameBoard getBoard() {
		return board;
	}

	@SuppressWarnings("unused")
	public void run() {

		boolean gameOver = false;
		board.setTime(Constants.GameDuration);
		room.notifyPlayers(board);
		try {
			sleep(500);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		for (int i = 0; i <= 3; i++) {
			board.resetSounds();
			int wait = 1200;
			switch (i) {
			case 0:
				state = State.Three;
				board.beepSound = true;
				break;
			case 1:
				state = State.Two;
				board.beepSound = true;
				break;
			case 2:
				state = State.One;
				board.beepSound = true;
				break;
			case 3:
				state = State.Zero;
				board.startSound = true;
				wait = 300;
				break;
			}
			board.setState(state);
			try {
				room.notifyPlayers(board);
				sleep(wait);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		state = State.Playing;
		board.setState(state);
		lastUpdate = startTime = System.currentTimeMillis();

		/* Initialize game objects */
		Vector<Tree> trees = board.getTrees();
		Vector<Wall> walls = board.getWalls();
		Vector<Water> waters = board.getWaters();
		Vector<Bullet> bullets = board.getBullets();
		Vector<Tank> tanks = board.getTanks();

		long lastTime = System.currentTimeMillis();
		long sumLoopTime = 0;
		int loopCnt = 0;
		board.resetSounds();

		while (!gameOver) {
			if (room.players.isEmpty()) {
				return;
			}
			lock.lock();
			long newTime = System.currentTimeMillis();
			long deltTime = newTime - lastTime;
			while (deltTime < Constants.GameCycle) {
				try {
					Thread.sleep(Constants.GameCycle - deltTime);
					newTime = System.currentTimeMillis();
					deltTime = newTime - lastTime;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			lastTime = newTime;
			sumLoopTime += deltTime;
			double dTime = (double) deltTime / 1000.0;
			loopCnt++;
			if (loopCnt >= 100) {
				System.out.println("Average loop time " + (sumLoopTime / loopCnt));
				loopCnt = 0;
				sumLoopTime = 0;
			}

			/* Clear tanks that quit */
			for (Tank t : tanks) {
				boolean is_matched = false;
				for (ServerClientCommunicator scc : room.players) {
					if (scc.getPlayerRecord().id == t.getID()) {
						is_matched = true;
						break;
					}
				}
				if (!is_matched) {
					t.destroy();
				}
			}

			// destroyObjects();
			for (Tank t : tanks) {
				t.decrementReload(deltTime);
			}
			respawnTanks(tanks, deltTime);
			moveTanks(tanks, walls, trees, waters, dTime);
			moveBullets(bullets, dTime);
			shootBullets();
			bulletsCollide(bullets);

			timeRemaining = Constants.GameDuration - (lastTime - startTime);
			board.setTime(timeRemaining);
			if (timeRemaining <= 0) {
				state = State.Finished;
				board.setState(state);
				gameOver = true;
				board.finish();
			}

			if (lastTime - lastUpdate >= Constants.GameSendCycle) {
				lastUpdate = lastTime;
				lock.lock();
				room.notifyPlayers(board);
				lock.unlock();
				board.resetSounds();
			}
		}

		/* Update player data */

		@SuppressWarnings("unchecked")
		Vector<Tank> sortCopy = (Vector<Tank>) tanks.clone();
		TankCompare comp = new TankCompare();
		Collections.sort(sortCopy, comp);

		Tank prevTank = null;
		int rank = 3;
		for (int index = 0; index < sortCopy.size(); ++index) {
			Tank curTank = sortCopy.elementAt(index);
			if (prevTank != null) {
				if (comp.compare(curTank, prevTank) != 0) {
					rank--;
				}
			}
			for (Tank t : tanks) {
				if (t.getID() == curTank.getID()) {
					t.setRank(rank);
					break;
				}
			}
			prevTank = curTank;
		}
		prevTank = null;
		sortCopy = null;
		comp = null;

		for (ServerClientCommunicator scc : room.players) {
			scc.setPlayerState(ServerClientCommunicator.State.IN_ROOM);
			PlayerRecord pr = scc.getPlayerRecord();
			for (Tank t : tanks) {
				if (t.getID() == pr.id) {
					System.out.println("User: " + pr.name);
					System.out.println("Score Before: " + pr.score);
					pr.score += Constants.rankScore[t.getRank()];
					System.out.println("Score After: " + pr.score);
					scc.updateScore(pr.score); // Send score to database
				}
			}
		}

		ServerPacket overPacket = new ServerPacket(ServerPacket.ResultType.OVER, board.getTanks(), "game over");
		String s = Util.serializeToString(overPacket);
		System.out.println("size of room "+room.players.size());
		for (ServerClientCommunicator scc : room.players) {
			System.out.println("sending game over results");
			scc.sendSerializedObject(s);
		}
		
		try {
			sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		room.gameOver();
		
		for (Tree tr : trees) {
			tr = null;
		}
		for (Wall wl : walls) {
			wl = null;
		}
		for (Water wr : waters) {
			wr = null;
		}
		for (Bullet b : bullets) {
			b = null;
		}
		for (Tank t : tanks) {
			t = null;
		}
		trees = null;
		walls = null;
		waters = null;
		bullets = null;
		tanks = null;
	}

	// Commented since never used
	// private void destroyObjects() {
	// Vector<Bullet> newBullets = new Vector<Bullet>();
	// for (Bullet bullet : board.getBullets()) {
	// if (!bullet.isDestroyed())
	// newBullets.add(bullet);
	// }
	// board.setBullets(newBullets);
	// }

	private void respawnTanks(Vector<Tank> tanks, long deltTime) {
		for (Tank t : tanks) {
			if (t.needRevive()) {
				Point2D.Double newPos = new Point2D.Double();
				randomPos(newPos);
				t.getShape().setPosition(newPos);
				t.setAngle(randomAngle());
				while (!tankPositionValid(t)) {
					randomPos(newPos);
					t.getShape().setPosition(newPos);
				}
			}
			if (!t.isAlive()) {
				t.setInvincible();
				t.decrementRespawn(deltTime);
			} else if (!t.isVulnerable()) {
				t.decrementInvincibility(deltTime);
			}
		}
	}

	private boolean tankPositionValid(Tank t) {
		for (Tank tank : board.getTanks()) {
			if (t.getMyPlayerRecord().id == tank.getMyPlayerRecord().id)
				continue;
			if (t.getShape().collidesWith(tank.getShape()))
				return false;
		}
		for (Wall wall : board.getWalls()) {
			if (t.getShape().collidesWith(wall.getShape()))
				return false;
		}
		for (Water water : board.getWaters()) {
			if (t.getShape().collidesWith(water.getShape()))
				return false;
		}
		for (Tree tree : board.getTrees()) {
			if (t.getShape().collidesWith(tree.getShape()))
				return false;
		}
		for (ObjectShape bound : board.getBoundaries()) {
			if (t.getShape().collidesWith(bound))
				return false;
		}
		return true;
	}

	private void randomPos(Point2D.Double pos) {
		int x = Constants.getRandomInt(Constants.GameBoardWidth);
		int y = Constants.getRandomInt(Constants.GameBoardHeight);
		pos.setLocation(x, y);
	}

	private int randomAngle() {
		return Constants.getRandomInt(360);
	}

	private void bulletsCollide(Vector<Bullet> bullets) {
		for (Bullet bullet : bullets) {
			for (Tank t : board.getTanks()) {
				if (t.isAlive())
					if (bullet.BulletCollide(t, board.getTanks()))
						board.hitTankSound = true;
			}
			for (Tree tr : board.getTrees()) {
				if (!tr.isDestroyed())
					if (bullet.BulletCollide(tr, board.getTanks()))
						board.hitTreeSound = true;
			}
			for (Wall wl : board.getWalls()) {
				if (bullet.BulletCollide(wl, board.getTanks()))
					board.hitWallSound = true;
			}
			double incidents[][] = { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 } };
			for (int i = 0; i < 4; i++) {
				if (bullet.collideBound(board.getBoundaries().get(i), incidents[i][0], incidents[i][1]))
					board.hitWallSound = true;
			}
		}
	}

	private void moveTanks(Vector<Tank> tanks, Vector<Wall> walls, Vector<Tree> trees, Vector<Water> waters,
			double dtime) {
		boolean canMove[] = new boolean[4];
		Vector<ObjectShape> newShapes = new Vector<ObjectShape>();
		Transformation trans[] = new Transformation[4];
		for (int i = 0; i < room.players.size(); i++) {
			trans[i] = transformationFromKeys(room.players.get(i).getKeyState());
			if (trans[i] != Transformation.Still) {
				newShapes.add(tanks.get(i).shapeAfter(dtime, trans[i]));
				canMove[i] = true;
			} else {
				newShapes.add(tanks.get(i).getShape());
				canMove[i] = false;
			}
		}
		// tank with tank
		for (int i = 1; i < tanks.size(); i++) {
			for (int j = 0; j < i; j++) {
				if (!tanks.get(i).isAlive() || !tanks.get(j).isAlive()) {
					continue;
				}
				if (!canMove[i] && !canMove[j])
					continue;
				if (newShapes.get(i).collidesWith(newShapes.get(j))) {
					canMove[i] = false;
					canMove[j] = false;
				}
			}
		}
		// tank with objects
		for (int i = 0; i < tanks.size(); i++) {
			for (Wall wall : walls) {
				if (!canMove[i])
					break;
				if (newShapes.get(i).collidesWith(wall.getShape())) {
					canMove[i] = false;
				}
			}
			for (Tree tree : trees) {
				if (tree.isDestroyed())
					continue;
				if (!canMove[i])
					break;
				if (newShapes.get(i).collidesWith(tree.getShape())) {
					canMove[i] = false;
				}
			}
			for (ObjectShape bound : board.getBoundaries()) {
				if (!canMove[i])
					break;
				if (newShapes.get(i).collidesWith(bound)) {
					canMove[i] = false;
				}
			}
			for (Water water : waters) {
				if (!canMove[i])
					break;
				if (newShapes.get(i).collidesWith(water.getShape())) {
					canMove[i] = false;
				}
			}
		}
		for (int i = 0; i < room.players.size(); i++) {
			if (canMove[i]) {
				tanks.get(i).updatePosition(dtime, trans[i]);
			} else {
				tanks.get(i).updatePosition(dtime, Transformation.Still);
			}
		}
	}

	private void moveBullets(Vector<Bullet> bullets, double dtime) {
		for (Bullet bullet : bullets) {
			if (bullet.isDestroyed())
				continue;
			bullet.updatePosition(dtime, Transformation.Forward);
		}
	}

	private Transformation transformationFromKeys(PlayerKeyState pks) {
		if (pks.up) {
			if (pks.right) {
				return Transformation.ForwardRight;
			} else if (pks.left) {
				return Transformation.ForwardLeft;
			} else {
				return Transformation.Forward;
			}
		} else if (pks.down) {
			if (pks.right) {
				return Transformation.BackwardRight;
			} else if (pks.left) {
				return Transformation.BackwardLeft;
			} else {
				return Transformation.Backward;
			}
		} else {
			if (pks.right) {
				return Transformation.Right;
			} else if (pks.left) {
				return Transformation.Left;
			} else {
				return Transformation.Still;
			}
		}
	}

	public void shootBullets() {
		for (int i = 0; i < room.players.size(); i++) {
			PlayerKeyState ks = room.players.get(i).getKeyState();
			if (ks.space) {
				ks.space = false;
				if (board.getTanks().get(i).isVulnerable()) {
					if (board.getTanks().get(i).getReload() <= 0) {
						Bullet bullet = board.getTanks().get(i).fireBullet();
						board.addBullet(bullet);
						board.shootSound = true;
						board.getTanks().get(i).setReload();
					}
				}
			}
		}
	}
}