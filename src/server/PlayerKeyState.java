package server;

public class PlayerKeyState {
  public boolean up, down, left, right, space;

  public PlayerKeyState() {
    up = down = left = right = space = false;
  }

  public void print() {
    if (up)
      System.out.println("UP ");
    if (down)
      System.out.println("Down ");
    if (right)
      System.out.println("RIGHT ");
    if (left)
      System.out.println("LEFT ");
    if (space)
      System.out.println("SPACE ");
  }
}
