package server;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

public class SendBoardThread extends Thread {
	
	private ServerClientCommunicator scc;
	private Lock lock;
	@SuppressWarnings("unused")
	private Condition cond;
	
	public SendBoardThread(ServerClientCommunicator _scc, Lock _lock, Condition _cond) {
		scc = _scc;
		lock = _lock;
		cond = _cond;
		start();
	}
	
	@Override
	public void run() {
		while (scc.thread_alive()) {
			lock.lock();
			//System.out.println("before wait");
			while (scc.toSend == null && scc.thread_alive()) {
				lock.unlock();
				yield();
				lock.lock();
			}
			String s = scc.toSend;
			scc.toSend = null;
			System.gc();
			lock.unlock();
			scc.sendSerializedObject(s);
		}
		System.out.println("send board thread dying");
	}
}
