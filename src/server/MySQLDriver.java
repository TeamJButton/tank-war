package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.mysql.jdbc.Driver;

import java.sql.ResultSet;

public class MySQLDriver {
  private Lock lock;
  private Connection con;
  private TankServer server;
  private final static String getPassPS = "SELECT pass FROM allUsers WHERE username=?";
  private final static String getScorePS = "SELECT score FROM allUsers WHERE username=? AND pass=?";
  private final static String addUserPS = "INSERT INTO allUsers(username,pass,score) VALUES(?,?,0)";
  private final static String updateScorePS = "UPDATE allUsers SET score=? WHERE username=?";

  public MySQLDriver(TankServer _server) {
    this.server = _server;
    try {
      new Driver();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    lock = new ReentrantLock();
  }

  public void connect() {
    try {
      con = DriverManager.getConnection("jdbc:mysql://localhost/JButton_Tank?autoReconnect=true", "tanker",
          "MillerJButton");
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void stop() {
    try {
      con.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private String password(String username) {
    lock.lock();
    connect();
    String pw = null;
    try {
      PreparedStatement ps = con.prepareStatement(getPassPS);
      ps.setString(1, username);
      ResultSet result = ps.executeQuery();
      if (result.next()) {
        pw = result.getString(1);
      }
      result.close();
      ps.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    stop();
    lock.unlock();
    return pw;
  }

  public boolean userExists(String username) {
    return password(username) != null;
  }

  public PlayerRecord createUser(String username, String pw) {
    PlayerRecord pr = null;
    if (userExists(username))
    	return null;
    lock.lock();
    connect();
    try {
      PreparedStatement ps = con.prepareStatement(addUserPS);
      ps.setString(1, username);
      ps.setString(2, pw);
      ps.execute();
      pr = new PlayerRecord(username, 0);
      ps.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    stop();
    lock.unlock();
    if (pr == null)
      System.out.println("FAIL");
    return pr;
  }

  public void updateScore(String username, int score) {
	  if (!userExists(username)) {
		  System.out.println(username+" does not exist, cannot update score");
		  return;
	  }
    lock.lock();
    connect();
    try {
      PreparedStatement ps = con.prepareStatement(updateScorePS);
      ps.setInt(1, score);
      ps.setString(2, username);
      ps.executeUpdate();
      server.addMessage("Database score updated for user " + username);
      ps.close();
    } catch (SQLException e) {
      server.addMessage("Database failed to update score for user " + username);
      e.printStackTrace();
    }
    stop();
    lock.unlock();
  }

  public int getScore(String username) {
    int score = -1;
    lock.lock();
    connect();
    try {
      PreparedStatement ps = con.prepareStatement(getScorePS);
      ps.setString(1, username);
      ResultSet result = ps.executeQuery();
      if (result.next()) {
        score = result.getInt(1);
      }
      result.close();
      ps.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    stop();
    lock.unlock();
    return score;
  }

  public PlayerRecord getPlayer(String username, String pw) {
    PlayerRecord pr = null;
    lock.lock();
    connect();
    try {
      PreparedStatement ps = con.prepareStatement(getScorePS);
      ps.setString(1, username);
      ps.setString(2, pw);
      ResultSet result = ps.executeQuery();
      if (result.next()) {
        pr = new PlayerRecord(username, result.getInt(1));
      }
      result.close();
      ps.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
    stop();
    lock.unlock();
    return pr;
  }
}
