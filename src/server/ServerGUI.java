package server;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import util.Constants;

public class ServerGUI extends JFrame {
//public class ServerGUI {
	private static final long serialVersionUID = 1113648077849752831L;
	private JTextArea messageTxA;
	
	public ServerGUI() {
		super("Tank War Server");
	    setLocationRelativeTo(null);
		this.setSize(Constants.ServerWidth, Constants.ServerHeight);
		messageTxA = new JTextArea();
		JScrollPane scroll = new JScrollPane(messageTxA);
		messageTxA.setEditable(false);
		this.add(scroll);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void addMessage(String msg) {
		messageTxA.append(msg+"\n");
		System.out.println(msg);
	}
}
