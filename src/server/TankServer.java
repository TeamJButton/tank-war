package server;

import java.io.IOException;
import java.util.Vector;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import networking.ServerPacket;
import networking.SignIn;

public class TankServer {

  private ServerListener serverListener;
  private MySQLDriver driver;
  private Vector<GameRoom> lobby;
  // private ServerGUI gui;
  private ServerConsole gui;
  private Lock lobby_lock;

  public TankServer() {
    lobby_lock = new ReentrantLock();
    lobby = new Vector<GameRoom>();
    // gui = new ServerGUI();
    gui = new ServerConsole();
    try {
      serverListener = new ServerListener(this);
    } catch (IOException e) {
      e.printStackTrace();
    }
    driver = new MySQLDriver(this);
  }

  public static void main(String[] argv) {
    new TankServer();
  }

  public void addMessage(String msg) {
    gui.addMessage(msg);
  }

  public GameRoom createRoom(ServerClientCommunicator scc, String roomName) {
    boolean exists = false;
    GameRoom room = null;
    lobby_lock.lock();
    for (int i = 0; i < lobby.size(); i++) {
      if (lobby.get(i).getRoomName().equals(roomName))
        exists = true;
    }
    if (!exists) {
      room = new GameRoom(this, roomName);
      room.addPlayer(scc);
      lobby.add(room);
    } else {
      ServerPacket packet = new ServerPacket(ServerPacket.ResultType.ROOM, false);
      scc.sendObject(packet);
    }
    lobby_lock.unlock();
    return room;
  }

  public void addRoom(GameRoom room) {
    lobby_lock.lock();
    lobby.add(room);
    lobby_lock.unlock();
    lobbyNotifyAll();
  }

  public void removeRoom(GameRoom room) {
    lobby_lock.lock();
    lobby.remove(room);
	System.gc();
    lobby_lock.unlock();
    lobbyNotifyAll();
  }

  public GameRoom joinRoom(int id, ServerClientCommunicator ssc) {
    GameRoom room = findRoom(id);
    if (room == null)
      return null;
    if (room.addPlayer(ssc)) {
      return room;
    }
    return null;
  }

  public void lobbyNotifyAll() {
    serverListener.lobbyNotifyAll(lobby);
  }

  public GameRoom findRoom(int id) {
    GameRoom room = null;
    lobby_lock.lock();
    for (int i = 0; i < lobby.size(); i++) {
      if (lobby.get(i).getRoomID() == id) {
        room = lobby.get(i);
        break;
      }
    }
    lobby_lock.unlock();
    return room;
  }

  public PlayerRecord userLogIn(SignIn signIn) {
    return driver.getPlayer(signIn.name, signIn.hash);
  }

  public PlayerRecord createUser(SignIn signIn) {
    return driver.createUser(signIn.name, signIn.hash);
  }

  public Vector<GameRoom> getLobby() {
    return lobby;
  }

  public void updateScore(String name, int new_score) {
    driver.updateScore(name, new_score);
  }
}
