package server;

import java.io.Serializable;

import gameObject.TankColor;
import util.Constants;

public class PlayerRecord implements Serializable {
  private static final long serialVersionUID = 6973948279413106932L;
  public String name;
  public int score;
  public boolean isGuest;
  public int id;
  public TankColor.Type color;
  private static int shared_id = 0;

  // Not a guest
  public PlayerRecord(String _name, int _score) {
    name = _name;
    score = _score;
    isGuest = false;
    color = TankColor.Type.Green;
    id = shared_id;
    shared_id++;
  }

  // Guest
  public PlayerRecord() {
    name = Constants.getRandomName();
    score = 0;
    isGuest = true;
    color = TankColor.Type.Green;
    id = shared_id;
    shared_id++;
  }
}
