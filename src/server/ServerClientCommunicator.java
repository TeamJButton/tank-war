package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gameObject.TankColor;
import networking.ClientPacket;
import networking.KeyPressedReleased;
import networking.ServerPacket;
import networking.SignIn;

public class ServerClientCommunicator extends Thread implements Serializable {
  private static final long serialVersionUID = 6597478635281051216L;
  transient private Socket socket;
  transient private ServerListener listener;
  transient private TankServer server;
  transient private boolean thread_alive;
  transient private ObjectOutputStream oos;
  transient private ObjectInputStream ois;
  transient private GameRoom room;
  private volatile boolean isCreator;
  private volatile boolean isGuest;
  private volatile boolean isReady;
  private volatile PlayerRecord playerRecord;
  transient private volatile PlayerKeyState keyState;
  transient volatile private Lock lock;
  transient volatile private Lock toSendLock;
  transient volatile private Condition toSendCond;
  transient volatile public String toSend;

  public enum State {
    NOT_LOGGED_IN, AT_LOBBY, IN_ROOM, IN_GAME
  }

  private State state;

  public ServerClientCommunicator(Socket _socket, ServerListener _listener, TankServer _server)
      throws IOException {
    socket = _socket;
    server = _server;
    listener = _listener;
    thread_alive = true;
    lock = new ReentrantLock();
    room = null;
    isReady = false;
    keyState = new PlayerKeyState();
    state = State.NOT_LOGGED_IN;
    oos = new ObjectOutputStream(socket.getOutputStream());
    oos.flush();
    ois = new ObjectInputStream(socket.getInputStream());
    printStatus("Connection Created");
    toSend = null;
    toSendLock = new ReentrantLock();
    toSendCond = toSendLock.newCondition();
    new SendBoardThread(this, toSendLock, toSendCond);
    start();
  }

  @Override
  public void run() {
    while (thread_alive) {
      try {
        Object readObject = ois.readObject();
        if (!(readObject instanceof ClientPacket)) {
          shutdown();
          return;
        }
        ClientPacket packet = (ClientPacket) readObject;
        if (state == State.NOT_LOGGED_IN) {
          if (packet.type == ClientPacket.ResultType.SIGNIN) {
            SignIn signIn = (SignIn) packet.object;
            ServerPacket result = playerSignIn(signIn);
            sendObject(result);
            if (result.success) {
              isGuest = false;
              state = State.AT_LOBBY;
              Vector<GameRoom> lobby = server.getLobby();
              ServerPacket lobbyResult =
                  new ServerPacket(ServerPacket.ResultType.LOBBY, lobby, "Welcome to the lobby");
              sendObject(lobbyResult);
              printStatus("User signed in");
            }
          } else if (packet.type == ClientPacket.ResultType.GUEST) {
            isGuest = true;
            state = State.AT_LOBBY;
            playerRecord = new PlayerRecord();
            ServerPacket result =
                new ServerPacket(ServerPacket.ResultType.SIGN, playerRecord, "Welcome guest");
            sendObject(result);
            Vector<GameRoom> lobby = server.getLobby();
            ServerPacket lobbyResult =
                new ServerPacket(ServerPacket.ResultType.LOBBY, lobby, "Welcome to the lobby");
            sendObject(lobbyResult);
            printStatus("Guest signed in");
          } else if (packet.type == ClientPacket.ResultType.END) {
            printStatus("Client Closed");
            shutdown();
          }
        } else if (state == State.AT_LOBBY) {

          if (packet.type == ClientPacket.ResultType.CREATE_ROOM) {
            isCreator = true;
            isReady = false;
            createRoom((String) packet.object);
            server.lobbyNotifyAll();
            printStatus("Create room");
          } else if (packet.type == ClientPacket.ResultType.JOIN_ROOM) {
            isCreator = false;
            isReady = false;
            int roomID = ((Integer) packet.object).intValue();
            joinRoom(roomID);
            server.lobbyNotifyAll();
            printStatus("Join room ID=" + roomID);
          } else if (packet.type == ClientPacket.ResultType.SIGNOUT) {
            state = State.NOT_LOGGED_IN;
            playerRecord = null;
            printStatus("Logged out");
          }

        } else if (state == State.IN_ROOM) {
          if (packet.type == ClientPacket.ResultType.READY) {
            isReady = true;
            room.checkStart();
            printStatus("Ready");
          } else if (packet.type == ClientPacket.ResultType.NOT_READY) {
            isReady = false;
            room.roomNotifyAll();
            printStatus("Not ready");
          } else if (packet.type == ClientPacket.ResultType.QUIT_ROOM) {
            room.removePlayer(this);
            room = null;
            server.lobbyNotifyAll();
            printStatus("Back to lobby");
          } else if (packet.type == ClientPacket.ResultType.COLOR) {
            playerRecord.color = ((TankColor.Type) packet.object);
            room.roomNotifyAll();
          } else if (packet.type == ClientPacket.ResultType.KICK) {
            Integer kickedID = (Integer) packet.object;
            room.kick(kickedID.intValue());
            server.lobbyNotifyAll();
            printStatus("Kick " + kickedID.intValue());
          }

        } else if (state == State.IN_GAME) {
          if (packet.type == ClientPacket.ResultType.KEY_PRESS) {
            KeyPressedReleased kp = (KeyPressedReleased) packet.object;
            boolean pressed = (kp.type == KeyPressedReleased.Type.Pressed);
            String msg;
            if (pressed) {
              msg = "Pressed ";
            } else {
              msg = "Released ";
            }
            switch (kp.key) {
              case UP:
                keyState.up = pressed;
                printStatus(msg + "up");
                break;
              case DOWN:
                keyState.down = (kp.type == KeyPressedReleased.Type.Pressed);
                printStatus(msg + "down");
                break;
              case LEFT:
                keyState.left = (kp.type == KeyPressedReleased.Type.Pressed);
                printStatus(msg + "left");
                break;
              case RIGHT:
                keyState.right = (kp.type == KeyPressedReleased.Type.Pressed);
                printStatus(msg + "right");
                break;
              case SPACE:
                keyState.space = true;
                printStatus(msg + "space");
                break;
            }
          }

        }
      } catch (ClassNotFoundException | IOException e) {
        e.printStackTrace();
        brokenPipe();
      }
    }
  }

  private ServerPacket playerSignIn(SignIn signIn) {
    if (listener.userLoggedIn(signIn.name)) {
      return new ServerPacket(ServerPacket.ResultType.SIGN, false);
    }
    PlayerRecord pr;
    if (signIn.signup) {
      pr = server.createUser(signIn);
    } else {
      pr = server.userLogIn(signIn);
    }
    playerRecord = pr;
    if (pr != null) {
      printStatus("Signed in successful");
      return new ServerPacket(ServerPacket.ResultType.SIGN, pr, "Login Successful");
    } else {
      printStatus("Sign in failed with username <" + signIn.name + ">");
      return new ServerPacket(ServerPacket.ResultType.SIGN, false);
    }
  }

  public void sendObject(ServerPacket packet) {
    lock.lock();
    try {
      String s = util.Util.serializeToString(packet);
      oos.writeObject(s);
      oos.flush();
    } catch (IOException e) {
      e.printStackTrace();
      brokenPipe();
    }
    lock.unlock();
  }

  public void brokenPipe() {
    thread_alive = false;
    toSendLock.lock();
    toSendCond.signalAll();
    toSendLock.unlock();
    listener.removeServerClientCommunicator(this);
    quitRoom();
    printStatus("Connection broken");
  }

  public void sendSerializedObject(String s) {
    if (s == null)
      return;
    if (s.length() == 0)
      return;
    lock.lock();
    try {
      oos.writeObject(s);
      oos.flush();
    } catch (IOException e) {
      e.printStackTrace();
      brokenPipe();
    }
    lock.unlock();
  }

  public void sendBoard(String s) {
    toSendLock.lock();
    toSend = s;
    toSendCond.signal();
    toSendLock.unlock();
  }

  public void createRoom(String roomName) {
    if (room != null) {
      ServerPacket result =
          new ServerPacket(ServerPacket.ResultType.ROOM, room, "Room already exists");
      sendObject(result);
    } else {
      room = server.createRoom(this, roomName);
      if (room != null) {
        state = State.IN_ROOM;
        server.lobbyNotifyAll();
      } else {
        ServerPacket result = new ServerPacket(ServerPacket.ResultType.ROOM, false);
        result.message = "Failed to create room";
        sendObject(result);
      }
    }
  }

  public void joinRoom(int id) {
    if (room == null) {
      room = server.joinRoom(id, this);
      if (room == null) {
        ServerPacket packet = new ServerPacket(ServerPacket.ResultType.ROOM, false);
        packet.message = "Failed to join room";
        sendObject(packet);
      } else {
        state = State.IN_ROOM;
      }
    }
  }

  public void setIsReady(boolean b) {
    isReady = b;
  }

  public void setIsCreator() {
    isCreator = true;
  }

  private void shutdown() {
    thread_alive = false;
    listener.removeServerClientCommunicator(this);
  }

  public boolean playerIsCreator() {
    return isCreator;
  }

  public boolean playerIsGuest() {
    return isGuest;
  }

  public PlayerKeyState getKeyState() {
    return keyState;
  }

  public boolean playerIsReady() {
    return isReady;
  }

  public State getPlayerState() {
    return state;
  }

  public void setPlayerState(State _state) {
    state = _state;
  }

  public PlayerRecord getPlayerRecord() {
    return playerRecord;
  }

  public String getPlayerName() {
    if (playerRecord != null)
      return playerRecord.name;
    return "";
  }

  public void printStatus(String msg) {
    if (playerRecord != null) {
      server.addMessage(
          socket.getInetAddress().getHostAddress() + "(" + playerRecord.name + "): " + msg);
    } else {
      server.addMessage(socket.getInetAddress().getHostAddress() + ": " + msg);
    }
  }

  public void quitRoom() {
    if (room != null) {
      room.removePlayer(this);
      room = null;
    }
    state = State.AT_LOBBY;
    server.lobbyNotifyAll();
  }

  public void updateScore(int score) {
    server.updateScore(this.getPlayerName(), score);
  }

  public boolean thread_alive() {
    return thread_alive;
  }
}
