create database JButton_Tank;
create user tanker;
grant all on tanker.* to 'tanker'@'localhost' identified by 'MillerJButton';
use JButton_Tank;
drop table if exists allUsers;
create table allUsers (
	username VARCHAR(30) NOT NULL,
    pass VARCHAR(64) NOT NULL,
    score int,
    PRIMARY KEY (username)
);